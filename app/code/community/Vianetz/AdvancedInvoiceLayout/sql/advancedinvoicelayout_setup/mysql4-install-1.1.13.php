<?php
/**
 * AdvancedInvoiceLayout Mysql Setup Class
 *
 * @category Vianetz
 * @package AdvancedInvoiceLayout
 * @author Christoph Massmann <C.Massmann@vianetz.com>
 * @license http://www.vianetz.com/license
 */

$this->startSetup();

$this->run("ALTER TABLE {$this->getTable('customer_group')} ADD advancedinvoicelayout_invoice_freetext TEXT NOT NULL DEFAULT '';");
$this->run("ALTER TABLE {$this->getTable('customer_group')} ADD advancedinvoicelayout_shipment_freetext TEXT NOT NULL DEFAULT '';");
$this->run("ALTER TABLE {$this->getTable('customer_group')} ADD advancedinvoicelayout_creditmemo_freetext TEXT NOT NULL DEFAULT '';");

$this->endSetup();


/* vim: set ts=4 sw=4 expandtab nu tw=90: */
