<?php
/**
 * AdvancedInvoiceLayout Order Shipment Pdf default items renderer
 *
 * @category   Vianetz
 * @package    Vianetz_AdvancedInvoiceLayout
 * @author     Christoph Massmann <C.Massmann@vianetz.com>
 * @license    http://www.vianetz.com/license
 */
class Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Shipment_Default extends Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Abstract
{
    /**
     * Draw item line
     */
    public function draw()
    {
        $order  = $this->getOrder();
        $item   = $this->getItem();
        $pdf    = $this->getPdf();
        $page   = $this->getPage();
        $shift  = array(0, 10, 0);
        
        $this->loadConfig($order->getStore());

        $this->_setFontRegular();
        $page->drawText($item->getQty()*1, $page->getWidth()-$this->margin_right-130,
$pdf->y, $this->charset);

        foreach (Mage::helper('core/string')->str_split($item->getName(), 55-$this->fontsize_regular*2, true, true) as $key => $part) {
            $page->drawText($part, $this->margin_left+108, $pdf->y-$shift[0],
$this->charset);
            $shift[0] += 10;
            /*if ($key > 0) {
                $shift[0] += 10;
            }*/
        }

        if ($options = $this->getItemOptions()) {
            foreach ($options as $option) {
                // draw options label
                $this->_setFontItalic();
                foreach (Mage::helper('core/string')->str_split(strip_tags($option['label']), 60,false,true) as $_option) {
                    $page->drawText($_option, $this->margin_left+108, $pdf->y-$shift[0],
$this->charset);
                    $shift[0] += 10;
                }
                // draw options value
                $this->_setFontRegular();
                if ($option['value']) {
                    $_printValue = isset($option['print_value']) ? $option['print_value']
: strip_tags($option['value']);
                    $values = explode(', ', $_printValue);
                    foreach ($values as $value) {
                        foreach (Mage::helper('core/string')->str_split($value, 60,true,true) as $_value) {
                            $page->drawText($_value, $this->margin_left+113,
$pdf->y-$shift[0], $this->charset);
                            $shift[0] += 10;
                        }
                    }
                }
            }
        }

        if (
Mage::getStoreConfig(self::XML_PATH_SALES_PDF_SHIPMENT_SHOW_PRODUCT_SHORTDESCRIPTION,
$order->getStore()) ) {
            $shift[0] += 4;
            $this->_setFontRegular($this->fontsize_regular-1);
            foreach
(Mage::helper('core/string')->str_split(strip_tags($this->_parseShortDescription()),
60-$this->fontsize_regular*2,
true, true) as $description) {
                $page->drawText(strip_tags($description), $this->margin_left+108,
$pdf->y-$shift[0], $this->charset);
                $shift[0] += 10;
            }
            $this->_setFontRegular();
        }

        foreach ($this->_parseDescription() as $description){
            $page->drawText(strip_tags($description), $this->margin_left+113,
$pdf->y-$shift{1}, $this->charset);
            $shift{1} += 10;
        }

        foreach (Mage::helper('core/string')->str_split($this->getSku($item), 15) as $key => $part) {
            if ($key > 0) {
                $shift[2] += 10;
            }
            $page->drawText($part, $this->margin_left+8, $pdf->y-$shift[2], $this->charset);
        }

        $pdf->y -=max($shift)+10;
    }
}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
