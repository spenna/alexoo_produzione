<?php
/**
 * AdvancedInvoiceLayout Order Shipment PDF model
 *
 * @category   Vianetz
 * @package    Vianetz_AdvancedInvoiceLayout
 * @author     Christoph Massmann <C.Massmann@vianetz.com>
 * @license    http://www.vianetz.com/license
 */
class Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Shipment extends Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract
{
    public function getPdf($shipments = array())
    {   
        $this->_beforeGetPdf();
        $this->_initRenderer('shipment');

        $this->pdf = new Zend_Pdf();
        $style = new Zend_Pdf_Style();
        $this->_setFontBold($style, 10);
        foreach ($shipments as $shipment) {
            if ($shipment->getStoreId()) {
                Mage::app()->getLocale()->emulate($shipment->getStoreId());
            }
            $this->page = $this->pdf->newPage(self::PDF_PAGE_FORMAT);
            $this->pdf->pages[] = $this->page;

            $order = $shipment->getOrder();
            
            $this->loadConfig($shipment->getStore());

            /*if ( ! $this->enabled ) {
                $mageShipment = new Mage_Sales_Model_Order_Pdf_Shipments();
                return $mageShipment->getPdf($shipments);
            }*/

            // Add image
            $this->insertLogo($this->page, $shipment->getStore());

            // Add address
            $this->insertAddress($this->page, $shipment->getStore());

            if (
Mage::getStoreConfig(self::XML_PATH_SALES_PDF_INVOICE_SWITCH_ADDRESSES,
$order->getStore())) {
                $this->insertShippingAndBillingAddresses($this->page, $order, false,
true);
            } else {
                $this->insertShippingAndBillingAddresses($this->page, $order);
            }

            $this->y -=50; 
            $this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            $this->_setFontRegular($this->page, $this->fontsize_regular+4);
            $this->page->drawText(Mage::helper('sales')->__('Packingslip # ') .
$shipment->getIncrementId(), $this->margin_left+6, $this->y, $this->charset);
            $this->_setFontRegular($this->page);
            $this->y -=10;
                        
            // Add Head
            $this->insertHeader($this->page, $order);

            // Add Order
            $this->insertOrder($this->page, $order, Mage::getStoreConfigFlag(self::XML_PATH_SALES_PDF_SHIPMENT_PUT_ORDER_ID, $order->getStoreId()));


            // Add table
            $this->_drawHeader($this->page);

            $this->y -=30;

            $this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

            // Add body
            foreach ($shipment->getAllItems() as $item){
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }

                $shift = 10;
                $shift = array();
                if ($this->y < $this->footer_y+50) {
					
                    // Add new table head
                    $this->page = $this->_newPage($this->pdf, $this->page, $item->getStore());

                    $this->_drawHeader($this->page);

                    $this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
                    $this->y -=20;
                }

                // Draw item
                $this->_drawItem($item, $this->page, $order);
            }

            $this->y -= 40;

            // Add Customer Comments
            if (
Mage::getStoreConfig(self::XML_PATH_SALES_PDF_SHIPMENT_SHOW_CUSTOMER_COMMENTS,
$shipment->getStore())) {
                foreach ( $shipment->getCommentsCollection() as $comment ) {
                    if ( $comment->getIsActive() ) { 
                        $this->insertFreeText($this->page, $this->pdf, $shipment->getStore(),
$comment->getComment()); 
                    }
                }
            }

		    if ( Mage::getStoreConfig(self::XML_PATH_SALES_PDF_SHIPMENT_SHOW_GIFTMSG,
$shipment->getStore())) {
                // Insert Gift Message
                $this->insertGiftMessage($this->page, $this->pdf, $shipment);
            }

            $this->y -= 40;

            // Insert free text
            $text = Mage::getStoreConfig(self::XML_PATH_SALES_PDF_SHIPMENT_FREETEXT,
$shipment->getStore());
            $text = preg_replace('/{{invoice_date(\+\d)?}}/e',
"Mage::helper('core')->formatDate(\$shipment->getCreatedAt().('\\1'!=''?'\\1'.' days':''))"
, $text);
            $this->insertFreeText($this->page, $this->pdf, $shipment->getStore(), $text);

        
            $this->insertFooter($this->page);
        }


        $this->_afterGetPdf();

        if ($shipment->getStoreId()) {
            Mage::app()->getLocale()->revert();
        }

        return $this->pdf;
    }

    protected function _drawHeader(&$page) {
        try {
            $page->setFillColor(new Zend_Pdf_Color_Html($this->header_color));
        }
        catch ( Exception $ex ) {
            $page->setFillColor(new Zend_Pdf_Color_Html($this->header_color_default));
        }
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle($this->margin_left, $this->y,
$page->getWidth()-$this->margin_right, $this->y-15);
        $this->y -=10;
        $this->_setFontRegular($page, $this->fontsize_regular-1);
        $page->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));
        $page->drawText(Mage::helper('sales')->__('SKU'), $this->margin_left+8, $this->y,
$this->charset);
        $page->drawText(Mage::helper('sales')->__('Products'), $this->margin_left+108,
$this->y, $this->charset);
        $page->drawText(Mage::helper('sales')->__('QTY'),
$page->getWidth()-$this->margin_right-130, $this->y, $this->charset);
    }
}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
