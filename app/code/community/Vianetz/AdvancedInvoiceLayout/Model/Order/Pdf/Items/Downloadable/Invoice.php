<?php
/**
 * AdvancedInvoiceLayout Invoice Downloadable Pdf Items renderer
 *
 * @category   Vianetz
 * @package    Vianetz_AdvancedInvoiceLayout
 * @author     Christoph Massmann <C.Massmann@vianetz.com>
 * @license    http://www.vianetz.com/license
 */
class Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Downloadable_Invoice extends Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Downloadable_Abstract
{
    public function draw()
    {
        $order  = $this->getOrder();
        $item   = $this->getItem();
        $pdf    = $this->getPdf();
        $page   = $this->getPage();
        $shift  = array(0, 10, 0);

        $this->loadConfig($pdf, $order->getStore());

        $leftBound = $this->margin_left+108;

        $font = $this->_setFontRegular();

        $page->drawText($item->getQty()*1,
$page->getWidth()-$this->margin_right-170-$pdf->widthForStringUsingFontSize($item->getQty()*1,
$font, $this->fontsize_regular), $pdf->y,
$this->charset);

        // in case Product name is longer than 80 chars - it is written in a few lines 
        foreach (Mage::helper('core/string')->str_split($item->getName(),
58-$this->fontsize_regular*2, true, true) as $key => $part) {
            $page->drawText($part, $this->margin_left+108, $pdf->y-$shift[0],
$this->charset);
            $shift[0] += 10;
        }

        if (
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Abstract::XML_PATH_SALES_PDF_INVOICE_SHOW_PRODUCT_SHORTDESCRIPTION,
$order->getStore()) ) {
            $shift[0] += 4;
            $this->_setFontRegular($this->fontsize_regular-1);
            foreach
(Mage::helper('core/string')->str_split(strip_tags($this->_parseShortDescription()),
60-$this->fontsize_regular*2,
true, true) as $description) {
                $page->drawText(strip_tags($description), $this->margin_left+108, $pdf->y-$shift[0],
$this->charset);
                $shift[0] += 10;
            }
            $this->_setFontRegular();
        }

        $options = $this->getItemOptions();
        if (isset($options)) {
            foreach ($options as $option) {
                // draw options label
                $this->_setFontItalic();
                foreach (Mage::helper('core/string')->str_split(strip_tags($option['label']), 60, false, true) as $_option) {
                    $page->drawText($_option, $this->margin_left+108, $pdf->y-$shift[0],
$this->charset);
                    $shift[0] += 10;
                }
                // draw options value
                $this->_setFontRegular();
                if ($option['value']) {
                    $_printValue = isset($option['print_value']) ? $option['print_value']
: strip_tags($option['value']);
                    $values = explode(', ', $_printValue);
                    foreach ($values as $value) {
                        foreach (Mage::helper('core/string')->str_split($value, 60,true,true) as $_value) {
                            $page->drawText($_value, $this->margin_left+110, $pdf->y-$shift[0],
$this->charset);
                            $shift[0] += 10;
                        }
                    }
                }
            }
        }

        foreach ($this->_parseDescription() as $description){
            $page->drawText(strip_tags($description), $this->margin_left+115, $pdf->y-$shift[1],
$this->charset);
            $shift[0] += 10;
        }

        /* in case Product SKU is longer than 36 chars - it is written in a few lines */
        foreach (Mage::helper('core/string')->str_split($this->getSku($item), 15) as $key => $part) {
            if ($key > 0) {
                $shift[2] += 10;
            }
            $page->drawText($part, $this->margin_left+8, $pdf->y-$shift[2],
$this->charset);
        }

        $font = $this->_setFontBold();

        $row_total = $order->formatPriceTxt($item->getRowTotal());
        $page->drawText($row_total, $page->getWidth()-$this->margin_right-5-$pdf->widthForStringUsingFontSize($row_total,
$font, $this->fontsize_bold), $pdf->y, $this->charset);

        $price = $order->formatPriceTxt($item->getPrice());
        $page->drawText($price,
$page->getWidth()-$this->margin_right-100-$pdf->widthForStringUsingFontSize($price, $font,
$this->fontsize_bold), $pdf->y, $this->charset);

        $tax = $order->formatPriceTxt($item->getTaxAmount());
        //$page->drawText($tax, 495-$pdf->widthForStringUsingFontSize($tax, $font, 9),
        //$pdf->y, $this->charset);

        // draw Links Section Title
        $this->_setFontItalic();
        $x = $leftBound;
        $_purchasedItems = $this->getLinks()->getPurchasedItems();
        $page->drawText($this->getLinksTitle(), $x, $pdf->y-$shift[0],
$this->charset);
        $shift[0] += 10;

        // draw Links
        $this->_setFontRegular(6);
        foreach ($_purchasedItems as $_link) {
//            $text = $_link->getLinkTitle() . ' ('.$_link->getNumberOfDownloadsUsed() . ' / ' . ($_link->getNumberOfDownloadsBought()?$_link->getNumberOfDownloadsBought():'U').')';
            $text = Mage::getUrl('downloadable/download/link', array('id' => $_link->getLinkHash(), '_secure' => true));

            $page->drawText($text, $x+10, $pdf->y-$shift[0],
$this->charset);
            $shift[0] += 10;
        }
        $pdf->y = $pdf->y - max($shift);
        $pdf->y -= 15;
    }
}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
