<?php
/**
 * AdvancedInvoiceLayout Abstract Downloadable Pdf Items renderer
 *
 * @category   Vianetz
 * @package    Vianetz_AdvancedInvoiceLayout
 * @author     Christoph Massmann <C.Massmann@vianetz.com>
 * @license    http://www.vianetz.com/license
 */
abstract class Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Downloadable_Abstract extends Mage_Downloadable_Model_Sales_Order_Pdf_Items_Abstract
{ 
	// Default fontsizes
	public $fontsize_regular = 10;
	public $fontsize_bold = 10;
	public $fontsize_italic = 10;

    public $margin_left = 0;
    public $margin_right = 0;

    public $header_color_default = "#EDEAEA";

    /**
     * HTML Color For Header
     */
    public $header_color;

    public $charset = "UTF-8";

	/**
	 * Function loadConfig
	 *
	 * Read admin values
	 */
	public function loadConfig(&$pdf, $store ) {
        $fontsize =
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_DEFAULT_FONTSIZE, $store);
        if ( $fontsize != null ) {
			$this->fontsize_regular = $fontsize;

			$this->fontsize_bold = $fontsize;

			$this->fontsize_italic = $fontsize;
		}

        $this->margin_left = 
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_MARGIN_LEFT,
$store);
        $this->margin_right = 
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_MARGIN_RIGHT,
$store);

        if (
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_CHARSET,
$store) != null ) {
            $this->charset = 
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_CHARSET, $store);
        }
    }

    protected function _parseShortDescription()
    {
        $product = Mage::getModel('catalog/product')->load($this->getItem()->getProductId());
        return $product->getShortDescription(); 
    }

    protected function _setFontRegular($size = null)
    {
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
        $this->getPage()->setFont($font, $size == null ? $this->fontsize_regular : $size);
        return $font;
    }
    
    protected function _setFontBold($size = null)
    {
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $this->getPage()->setFont($font, $size == null ? $this->fontsize_bold : $size);
        return $font;
    }

    protected function _setFontItalic($size = null)
    {
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_ITALIC);
        $this->getPage()->setFont($font, $size == null ? $this->fontsize_italic : $size);
        return $font;
    }
}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
