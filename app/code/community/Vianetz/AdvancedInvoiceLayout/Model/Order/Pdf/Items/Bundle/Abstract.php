<?php
/**
 * AdvancedInvoiceLayout Abstract Bundle Pdf Items renderer
 *
 * @category   Vianetz
 * @package    Vianetz_AdvancedInvoiceLayout
 * @author     Christoph Massmann <C.Massmann@vianetz.com>
 * @license    http://www.vianetz.com/license
 */
abstract class Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Bundle_Abstract extends Mage_Bundle_Model_Sales_Order_Pdf_Items_Abstract
{
    /**
     * Default fontsizes scaling factor
     * @var float
     */
	public $fontsize_regular = 10;
	public $fontsize_bold = 10;
	public $fontsize_italic = 10;

    public $margin_left = 0;
    public $margin_right = 0;

    public $header_color_default = "#EDEAEA";

    public $charset = "UTF-8";

    /**
     * HTML Color For Header
     */
    public $header_color;

	/**
	 * Function loadConfig
	 *
	 * Read admin values
	 */
	public function loadConfig(&$pdf, $store ) {
        $fontsize =
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_DEFAULT_FONTSIZE, $store);
        if ( $fontsize != null ) {
			$this->fontsize_regular = $fontsize;

			$this->fontsize_bold = $fontsize;

			$this->fontsize_italic = $fontsize;
		}
        // Calculate footer padding
       $pdf->footer_y =
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_MARGIN_BOTTOM,
$store);

    	$pdf->footer_y +=
20+max(substr_count(Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_FOOTERTEXT_1COLUMN,
$store), "\n"),
substr_count(Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_FOOTERTEXT_2COLUMN,
$store), "\n"),
substr_count(Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_FOOTERTEXT_3COLUMN,
$store), "\n"))*10;

        $this->margin_left =
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_MARGIN_LEFT);
        $this->margin_right =
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_MARGIN_RIGHT);

        $this->header_color =
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_HEADER_COLOR);

        if (
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_HEADER_COLOR)
!= null ) {
            $this->charset =
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_CHARSET);
        }
    }

    /**
     * Create new PDF Page
     * @access private
     * @param Zend_Pdf $pdf
     * @param Zend_Pdf_Page $page
     * @param string $store
     */
	protected function _newPage(&$pdf, $page, $store) {
        $this->insertFooter($pdf, $page, $store);

        if (
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_MARGIN_TOP) == ""
)
            $padding = 0;
        else
            $padding =
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_MARGIN_TOP);

        $page =
$pdf->pdf->newPage(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::PDF_PAGE_FORMAT);
        $pdf->pdf->pages[] = $page;
        $this->setPage($page);
        $pdf->y = $page->getHeight()-$padding;
        $this->_setFontRegular();
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

		return $page;
	}

    /**
     * Insert Footer with company legal and payment informations on bottom of document.
     * @param $pdf
     * @param Zend_Pdf_Page $page
     * @param string $store Store Id
     */
    protected function insertFooter(&$pdf, $page, $store = null) {

            $_column1 =
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_FOOTERTEXT_1COLUMN,
$store);
            $_column2 =
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_FOOTERTEXT_2COLUMN,
$store);
            $_column3 =
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract::XML_PATH_SALES_PDF_ADVANCEDINVOICELAYOUT_FOOTERTEXT_3COLUMN,
$store);

			// Draw line
			$page->setLineWidth(0.5);
			$page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
			if ( trim($_column1) != "" || trim($_column2) != "" || trim($_column3) != "" )
				$page->drawLine(66, $pdf->footer_y+10, 550, $pdf->footer_y+10);
			
			// Draw first column from admin
			$pdf->y = $pdf->footer_y;
			$pdf->x = 76;   
			$font = $this->_setFontRegular(6);
			foreach (explode("\n", $_column1) as $value ) {
				$page->drawText(trim(strip_tags($value)), $pdf->x, $pdf->y,
$this->charset); // 45
				$pdf->y -=10;
			}
			
			// Draw second column from admin
            if ( trim($_column2) != "" ) {
			    $pdf->y = $pdf->footer_y;
                $pdf->x +=170;
			    foreach (explode("\n", $_column2) as $value){
				    $page->drawText(trim(strip_tags($value)), $pdf->x, $pdf->y,
$this->charset); // 45
				    $pdf->y -=10;
			    }
                $pdf->x +=180;
            } else {
                $pdf->x +=260;
            }

			// Draw third column from admin
			$pdf->y = $pdf->footer_y;
			$font = $this->_setFontRegular(6);
			foreach (explode("\n", $_column3) as $value ) {
				$page->drawText(trim(strip_tags($value)), $pdf->x, $pdf->y,
$this->charset); // 45
				$pdf->y -=10;
			}
    }


    /**
     * Draw Headline Of Table
     * @param $pdf
     * @param Zend_Pdf_Page $page
     */
    protected function _drawHeader(&$pdf, Zend_Pdf_Page $page)
    {
        $font = $this->_setFontRegular($this->fontsize_regular-1);
        $font = $page->getFont();
        $size = $page->getFontSize();

        try {
            $page->setFillColor(new Zend_Pdf_Color_Html($this->header_color));
        }
        catch ( Exception $ex ) {
            $page->setFillColor(new Zend_Pdf_Color_Html($this->header_color_default));
        }
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle($this->margin_left, $pdf->y,
$page->getWidth()-$this->margin_right, $pdf->y-15);
        $pdf->y -=10;

        $page->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));
        $page->drawText(Mage::helper('sales')->__('SKU'), $this->margin_left+8, $pdf->y,
$this->charset);
        $page->drawText(Mage::helper('sales')->__('Product'), $this->margin_left+118, $pdf->y,
$this->charset);
        $page->drawText(Mage::helper('sales')->__('QTY'), 390, $pdf->y,
$this->charset);
        $page->drawText(Mage::helper('sales')->__('Price'), 440, $pdf->y,
$this->charset);
        //$page->drawText(Mage::helper('sales')->__('Tax'), 480, $this->y,
//$this->charset);
        $page->drawText(Mage::helper('sales')->__('Subtotal'),
$page->getWidth()-$this->margin_right-5-$pdf->widthForStringUsingFontSize(Mage::helper('sales')->__('Subtotal'), $font,
$this->fontsize_regular-1), $pdf->y, $this->charset);
    }

    protected function _parseShortDescription()
    {
        $product = Mage::getModel('catalog/product')->load($this->getItem()->getProductId());
        return $product->getShortDescription(); 
    }

    protected function _parseItemShortDescription($item)
    {
        $product = Mage::getModel('catalog/product')->load($item->getProductId());
        return $product->getShortDescription(); 
    }

    protected function _setFontRegular($size = null)
    {
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
        $this->getPage()->setFont($font, $size == null ? $this->fontsize_regular : $size);
        return $font;
    }
    
    protected function _setFontBold($size = null)
    {
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
        $this->getPage()->setFont($font, $size == null ? $this->fontsize_bold : $size);
        return $font;
    }

    protected function _setFontItalic($size = null)
    {
        $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_ITALIC);
        $this->getPage()->setFont($font, $size == null ? $this->fontsize_italic : $size);
        return $font;
    }
}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
