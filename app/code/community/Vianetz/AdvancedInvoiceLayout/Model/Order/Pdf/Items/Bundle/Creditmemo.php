<?php
/**
 * AdvancedInvoiceLayout Creditmemo Bundle Pdf Items renderer
 *
 * @category   Vianetz
 * @package    Vianetz_AdvancedInvoiceLayout
 * @author     Christoph Massmann <C.Massmann@vianetz.com>
 * @license    http://www.vianetz.com/license
 */
class Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Bundle_Creditmemo extends Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Bundle_Abstract
{
    public function draw()
    {
        $order  = $this->getOrder();
        $item   = $this->getItem();
        $pdf    = $this->getPdf();
        $page   = $this->getPage();

        $items = $this->getChilds($item);
        $_prevOptionId = '';
        $shift  = array(0, 0, 0);
        $size = 7;

        $this->loadConfig($pdf, $order->getStore());

        $leftBound  =  $this->margin_left+8;
        $rightBound = $page->getWidth()-$this->margin_right-5;

        foreach ($items as $_item) {
            $this->_setFontRegular();
            $x = $leftBound;

            if ($pdf->y-$shift[1] < $pdf->footer_y+30) {
                $pdf->page = $this->_newPage($pdf, $pdf->page, $order->getStore());
            }
            
            // draw SKUs
            $shift[2] = 0;
            if (!$_item->getOrderItem()->getParentItem()) {
                foreach (Mage::helper('core/string')->str_split($item->getSku(), 10) as $key => $part) {
                    if ($key > 0) {
                        $shift[2] += 10;
                    }
                    $pdf->page->drawText($part, $x, $pdf->y - $shift[2],
$this->charset);
                }
            }
            $x += 70;

            // draw selection attributes
            if ($_item->getOrderItem()->getParentItem()) {
                $attributes = $this->getSelectionAttributes($_item);
                if ($_prevOptionId != $attributes['option_id']) {
                    $this->_setfontItalic();
                    foreach
(Mage::helper('core/string')->str_split($attributes['option_label'],58-$this->fontsize_regular*2,
true, true) as $key => $part) {
                        if ($key > 0) {
                            $shift[2] += 10;
                        }
                        $pdf->page->drawText($part, $x, $pdf->y - $shift[2],
$this->charset);
                    }
                    $this->_setFontRegular();
                    $_prevOptionId = $attributes['option_id'];
                    $pdf->y -= 10;
                }
            }

            // draw product titles
            if ($_item->getOrderItem()->getParentItem()) {
                $feed = $x + 5;
                $name = $this->getValueHtml($_item);
                $shortdesc = $this->_parseItemShortDescription($_item);
            } else {
                $feed = $x;
                $name = $_item->getName();
                $shortdesc = $this->_parseShortDescription();
            }
            foreach (Mage::helper('core/string')->str_split($name,
58-$this->fontsize_regular*2, true, true) as $key => $part) {
                $pdf->page->drawText($part, $feed, $pdf->y,
$this->charset);
                $pdf->y -= 10;
            }

            if (
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Abstract::XML_PATH_SALES_PDF_CREDITMEMO_SHOW_PRODUCT_SHORTDESCRIPTION,
$order->getStore()) ) {
                $shift[0] += 4;
                $this->_setFontRegular($this->fontsize_regular-1);
                foreach
(Mage::helper('core/string')->str_split(strip_tags($shortdesc),
60-$this->fontsize_regular*2,
true, true) as $description) {
                    $shift[0] += 10;
                    $pdf->page->drawText(strip_tags($description), $feed, $pdf->y-$shift[0],
$this->charset);
                }
                $this->_setFontRegular();
            }
            $x += 190;


            $font = $this->_setFontBold();
            if ($this->canShowPriceInfo($_item)) {
            
                // draw QTY
                $text = $_item->getQty() * 1;
                $pdf->page->drawText($text, $pdf->getAlignCenter($text, $x, 30, $font,
$size), $pdf->y, $this->charset);
                $x += 50;
                
                // draw Total(ex)
                $text = $order->formatPriceTxt($_item->getRowTotal());
                //$page->drawText($text, $pdf->getAlignRight($text, $x, 50, $font, $size),
                //$pdf->y, $this->charset);
                $x += 0;

                // draw Discount
                $text = $order->formatPriceTxt(-$_item->getDiscountAmount());
                $pdf->page->drawText($text, $page->getWidth()-$this->margin_right-115-$pdf->widthForStringUsingFontSize($text, $font,
$this->fontsize_regular), $pdf->y, $this->charset);
                $x += 90;

                // draw Tax
                $text = $order->formatPriceTxt($_item->getTaxAmount());
                //$page->drawText($text, $pdf->getAlignRight($text, $x, 45, $font, $size,
                //10), $pdf->y, $this->charset);
                $x += 0;

                // draw Total(inc)
                $text = $order->formatPriceTxt($_item->getRowTotal()+$_item->getTaxAmount()-$_item->getDiscountAmount());
                $pdf->page->drawText($text,
$rightBound-$pdf->widthForStringUsingFontSize($text, $font, $this->fontsize_regular),
$pdf->y, $this->charset);
            }

            $pdf->y -=max($shift)+10;
        }

        // custom options
        $options = $item->getOrderItem()->getProductOptions();
        if ($options || $item->getOrderItem()->getDescription()) {
            $this->_setFontRegular();
            if (isset($options['options'])) {
                foreach ($options['options'] as $option) {
                    $this->_setFontItalic();
                    foreach (Mage::helper('core/string')->str_split(strip_tags($option['label']), 50, false, true) as $_option) {
                        $pdf->page->drawText($_option, $leftBound+70, $pdf->y,
$this->charset);
                        $pdf -= 10;
                    }
                    $this->_setFontRegular();
                    if ($option['value']) {
                        $_printValue = isset($option['print_value']) ?
$option['print_value'] : strip_tags($option['value']);
                        $values = explode(', ', $_printValue);
                        foreach ($values as $value) {
                            foreach (Mage::helper('core/string')->str_split($value, 50,true,true) as $_value) {
                                $pdf->page->drawText($_value, $leftBound + 75,
$pdf->y-$shift[1], $this->charset);
                                $shift[1] += 10;
                            }
                        }
                    }
                }
            }

            foreach ($this->_parseDescription() as $description){
                $pdf->page->drawText(strip_tags($description), $leftBound + 75,
$pdf->y-$shift{1}, $this->charset);
                $shift[1] += 10;
            }

            $pdf->y -= max($shift)+10;
        }
    }
}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
