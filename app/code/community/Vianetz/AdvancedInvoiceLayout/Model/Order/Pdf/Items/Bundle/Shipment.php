<?php
/**
 * AdvancedInvoiceLayout Shipment Bundle Pdf Items renderer
 *
 * @category   Vianetz
 * @package    Vianetz_AdvancedInvoiceLayout
 * @author     Christoph Massmann <C.Massmann@vianetz.com>
 * @license    http://www.vianetz.com/license
 */
class Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Bundle_Shipment extends Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Bundle_Abstract
{
    /**
     * Draw item line
     */
    public function draw()
    {
        $order  = $this->getOrder();
        $item   = $this->getItem();
        $pdf    = $this->getPdf();
        $page   = $this->getPage();

        $this->loadConfig($pdf, $order->getStore());

        $this->_setFontRegular();

        $shipItems = $this->getChilds($item);
        $items = array_merge(array($item->getOrderItem()), $item->getOrderItem()->getChildrenItems());

        $_prevOptionId = '';

        foreach ($items as $_item) {
            $shift  = array(0, 10, 0);

            $attributes = $this->getSelectionAttributes($_item);

            if ($pdf->y-$shift[1] < $pdf->footer_y+30) {
                $pdf->page = $this->_newPage($pdf, $pdf->page, $order->getStore());
            }

            if ($_item->getParentItem()) {
                if ($_prevOptionId != $attributes['option_id']) {
                    $this->_setFontItalic();
                    foreach
(Mage::helper('core/string')->str_split($attributes['option_label'],58-$this->fontsize_regular*2,
true, true) as $key => $part) {
                        $pdf->y -= 10;
                        $pdf->page->drawText($part, $this->margin_left+108, $pdf->y,
$this->charset);
                    }
                    $this->_setFontRegular();
                    $_prevOptionId = $attributes['option_id'];
                    $pdf->y -= 10;
                }
            }

            if (($this->isShipmentSeparately() && $_item->getParentItem()) || (!$this->isShipmentSeparately() && !$_item->getParentItem())) {
                if (isset($shipItems[$_item->getId()])) {
                    $qty = $shipItems[$_item->getId()]->getQty()*1;
                } else if ($_item->getIsVirtual()) {
                    $qty = Mage::helper('bundle')->__('N/A');
                } else {
                    $qty = 0;
                }
            } else {
                $qty = '';
            }

            $pdf->page->drawText($qty, $page->getWidth()-$this->margin_right-130, $pdf->y,
$this->charset);

            if ($_item->getParentItem()) {
                $feed = $this->margin_left+113;
                $name = $this->getValueHtml($_item);
                $shortdesc = $this->_parseItemShortDescription($_item);
            } else {
                $feed = $this->margin_left+108;
                $name = $_item->getName();
                $shortdesc = $this->_parseShortDescription();
            }
            foreach (Mage::helper('core/string')->str_split($name,
58-$this->fontsize_regular*2, true, true) as $key => $part) {
                $pdf->page->drawText($part, $feed, $pdf->y-$shift[0],
$this->charset);
                $shift[0] += 10;
            }

            if (
Mage::getStoreConfig(Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Items_Abstract::XML_PATH_SALES_PDF_SHIPMENT_SHOW_PRODUCT_SHORTDESCRIPTION,
$order->getStore()) ) {
                $shift[0] += 4;
                $this->_setFontRegular($this->fontsize_regular-1);
                foreach
(Mage::helper('core/string')->str_split(strip_tags($shortdesc),
60-$this->fontsize_regular*2,
true, true) as $description) {
                    $shift[0] += 10;
                    $pdf->page->drawText(strip_tags($description), $feed, $pdf->y-$shift[0],
$this->charset);
                }
                $this->_setFontRegular();
            }

            foreach (Mage::helper('core/string')->str_split($item->getSku(), 15) as $key => $part) {
                if ($key > 0) {
                    $shift[2] += 10;
                }
                $pdf->page->drawText($part, $this->margin_left+8, $pdf->y-$shift[2],
$this->charset);
            }

            $pdf->y -=max($shift)+10;
        }

        if ($item->getOrderItem()->getProductOptions() || $item->getOrderItem()->getDescription()) {
            $shift[1] = 10;
            $options = $item->getOrderItem()->getProductOptions();
            if (isset($options['options'])) {
                foreach ($options['options'] as $option) {
                    $this->_setFontItalic();
                    foreach (Mage::helper('core/string')->str_split(strip_tags($option['label']), 60,false,true) as $_option) {
                        $pdf->page->drawText($_option, $this->margin_left+108, $pdf->y-$shift[1],
$this->charset);
                        $shift[1] += 10;
                    }

                    $this->_setFontRegular();

                    if ($option['value']) {
                        $_printValue = isset($option['print_value']) ?
$option['print_value'] : strip_tags($option['value']);
                        $values = explode(', ', $_printValue);
                        foreach ($values as $value) {
                            foreach (Mage::helper('core/string')->str_split($value, 70,true,true) as $_value) {
                                $pdf->page->drawText($_value, $this->margin_left+113, $pdf->y-$shift[1],
$this->charset);
                                $shift[1] += 10;
                            }
                        }
                    }
                }
            }

            foreach ($this->_parseDescription() as $description){
                $pdf->page->drawText(strip_tags($description), $this->margin_left+113, $pdf->y-$shift{1},
$this->charset);
                $shift{1} += 10;
            }

            $pdf->y -= max($shift)+10;
        }
    }
}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
