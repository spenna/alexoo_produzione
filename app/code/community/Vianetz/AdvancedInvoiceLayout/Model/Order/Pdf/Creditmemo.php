<?php
/**
 * AdvancedInvoiceLayout Order Creditmemo PDF model
 *
 * @category   Vianetz
 * @package    Vianetz_AdvancedInvoiceLayout
 * @author     Christoph Massmann <C.Massmann@vianetz.com>
 * @license    http://www.vianetz.com/license
 */
class Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Creditmemo extends Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract
{
    public function getPdf($creditmemos = array())
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('creditmemo');

        $this->pdf = new Zend_Pdf();
        $style = new Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        foreach ($creditmemos as $creditmemo) {
            if ($creditmemo->getStoreId()) {
                Mage::app()->getLocale()->emulate($creditmemo->getStoreId());
            }
            $this->page = $this->pdf->newPage(self::PDF_PAGE_FORMAT);
            $this->pdf->pages[] = $this->page;

            $order = $creditmemo->getOrder();

            $this->loadConfig($creditmemo->getStore());

            /*if ( ! $this->enabled ) {
                $mageCreditmemo = new Mage_Sales_Model_Order_Pdf_Creditmemo();
                return $mageCreditmemo->getPdf($creditmemos);
            }*/

            // Add image
            $this->insertLogo($this->page, $creditmemo->getStore());

            // Add address
            $this->insertAddress($this->page, $creditmemo->getStore());

            if (
Mage::getStoreConfig(self::XML_PATH_SALES_PDF_INVOICE_SWITCH_ADDRESSES,
$order->getStore())) {
                $this->insertShippingAndBillingAddresses($this->page, $order, false,
true);
            } else {
                $this->insertShippingAndBillingAddresses($this->page, $order);
            }

            $this->y -=50; 
            $this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            $this->_setFontRegular($this->page, $this->fontsize_regular+4);
            $this->page->drawText(Mage::helper('sales')->__('Credit Memo # ') .
$creditmemo->getIncrementId(), $this->margin_left+6, $this->y, $this->charset);
            $this->_setFontRegular($this->page);
            $this->y -=22;

            // Creditmemo date
            $this->page->drawText(Mage::helper('sales')->__('Creditmemo Date: ') .
Mage::helper('core')->formatDate($creditmemo->getCreatedAt(), 'medium', false),
$this->margin_left+6, $this->y, $this->charset);
                        
            // Add Head
            $this->insertHeader($this->page, $order);

            // Add Order
            $this->insertOrder($this->page, $order, Mage::getStoreConfigFlag(self::XML_PATH_SALES_PDF_CREDITMEMO_PUT_ORDER_ID, $order->getStoreId()));    


            // Add table head
            $this->_drawHeader($this->page);
            $this->y -=30;

            $this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

            // Add body
            foreach ($creditmemo->getAllItems() as $item){
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }
                
                // Draw item
                $this->_drawItem($item, $this->page, $order);

                $shift = array();
                if ($this->y < $this->footer_y+140) {
					
                    // Add new table head
                    $this->page = $this->_newPage($this->pdf, $this->page, $item->getStore());
                    
                    $this->_drawHeader($this->page);
                    $this->y -=20;
                }
            }
           
            $this->y -= 40;
            if ($this->y < $this->footer_y+60) {
				$this->page = $this->_newPage($this->pdf, $this->page,
$creditmemo->getStoreId());
				$font = $this->_setFontRegular($this->page, 9);
				$this->y -=40;
            }

            // Add totals
            $this->insertTotals($this->page, $creditmemo);

            $this->y -=40;

            // Add Customer Comments
            if (
Mage::getStoreConfig(self::XML_PATH_SALES_PDF_CREDITMEMO_SHOW_CUSTOMER_COMMENTS,
$creditmemo->getStore())) {
                foreach ( $creditmemo->getCommentsCollection() as $comment ) {
                    if ( $comment->getIsActive() ) { 
                        $this->insertFreeText($this->page, $this->pdf, $creditmemo->getStore(),
$comment->getComment()); 
                    }
                }
            }

            $this->y -=20;
            // Insert free text
            $text = Mage::getStoreConfig(self::XML_PATH_SALES_PDF_CREDITMEMO_FREETEXT,
$creditmemo->getStoreId());
            $text = preg_replace('/{{invoice_date(\+\d)?}}/e',
"Mage::helper('core')->formatDate(\$creditmemo->getCreatedAt().('\\1'!=''?'\\1'.' days':''))"
    , $text);
            $this->insertFreeText($this->page, $this->pdf, $creditmemo->getStoreId(), $text);


            $this->insertFooter($this->page);
        }
        
        $this->_afterGetPdf();

        if ($creditmemo->getStoreId()) {
            Mage::app()->getLocale()->revert();
        }

        return $this->pdf;
    }

    protected function _drawHeader(Zend_Pdf_Page $page)
    {
        $this->_setFontRegular($page, $this->fontsize_regular-1);
        $font = $page->getFont();
        $size = $page->getFontSize();

        try {
            $page->setFillColor(new Zend_Pdf_Color_Html($this->header_color));
        }
        catch ( Exception $ex ) {
            $page->setFillColor(new Zend_Pdf_Color_Html($this->header_color_default));
        }
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle($this->margin_left, $this->y,
$page->getWidth()-$this->margin_right, $this->y-15);
        $this->y -=10;
        $page->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));

        $page->drawText(Mage::helper('sales')->__('SKU'), $x = $this->margin_left+8,
$this->y, $this->charset);
        $x += 70;

        $page->drawText(Mage::helper('sales')->__('Products'), $x, $this->y,
$this->charset);
        $x += 190;
        
        $text = Mage::helper('sales')->__('QTY');
        $page->drawText($text, $this->getAlignCenter($text, $x, 30, $font, $size),
$this->y, $this->charset);
        $x += 50;

        $text = Mage::helper('sales')->__('Total(ex)');
        //$page->drawText($text, $this->getAlignRight($text, $x, 50, $font, $size),
        //$this->y, $this->charset);
        $x += 0;

        $text = Mage::helper('sales')->__('Discount');
        $page->drawText($text, $page->getWidth()-$this->margin_right-115-$this->widthForStringUsingFontSize($text, $font,
$this->fontsize_regular-1), $this->y, $this->charset);
        $x += 90;

        $text = Mage::helper('sales')->__('Tax');
        //$page->drawText($text, $this->getAlignRight($text, $x, 45, $font, $size,
        //$this->fontsize_bold), $this->y, $this->charset);
        $x += 0;

        $text = Mage::helper('sales')->__('Total(inc)');
        $page->drawText($text,
$page->getWidth()-$this->margin_right-5-$this->widthForStringUsingFontSize($text, $font,
$this->fontsize_regular-1), $this->y, $this->charset);
    }
}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
