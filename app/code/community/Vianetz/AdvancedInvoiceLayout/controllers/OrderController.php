<?php
/**
 * AdvancedInvoiceLayout Order Controller
 * 
 * @category Vianetz
 * @package AdvancedInvoiceLayout
 * @author Christoph Massmann <C.Massmann@vianetz.com>
 * @license http://www.vianetz.com/license
 */

require_once 'Mage/Sales/controllers/OrderController.php';

class Vianetz_AdvancedInvoiceLayout_OrderController extends Mage_Sales_OrderController 
{
    public function printInvoiceAction()
    {
        $invoiceId = (int) $this->getRequest()->getParam('invoice_id');

        if ($invoiceId) {
            $invoice = Mage::getModel('sales/order_invoice')->load($invoiceId);
            $order = $invoice->getOrder();
        } else {
            $orderId = (int) $this->getRequest()->getParam('order_id');
            $order = Mage::getModel('sales/order')->load($orderId);
        }

        if ($this->_canViewOrder($order)) {
            if ($order->getStoreId()) {
                Mage::app()->setCurrentStore($order->getStoreId());
            }

            if (! isset($invoice) ) {
                $invoiceIds = array();
                foreach ($order->getInvoiceCollection() as $_invoice) {
                    $invoiceIds[] = $_invoice->getId();
                }
                $invoices = Mage::getResourceModel('sales/order_invoice_collection')
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('entity_id', array('in' => $invoiceIds))
                    ->load();
                if (!isset($pdf)){
                    $pdf = Mage::getModel('sales/order_pdf_invoice')->getPdf($invoices);
                } else {
                    $pages = Mage::getModel('sales/order_pdf_invoice')->getPdf($invoices);
                    $pdf->pages = array_merge ($pdf->pages, $pages->pages);
                }
            } else {
                $pdf = Mage::getModel('sales/order_pdf_invoice')->getPdf(array($invoice));
            }

            $this->_prepareDownloadResponse('invoice'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(), 'application/pdf');
        } else {
            $this->_redirect('*/*/history');
        }
    }

    public function printShipmentAction()
    {
        $shipmentId = (int) $this->getRequest()->getParam('shipment_id');

        if ($shipmentId) {
            $shipment = Mage::getModel('sales/order_shipment')->load($shipmentId);
            $order = $shipment->getOrder();
        } else {
            $orderId = (int) $this->getRequest()->getParam('order_id');
            $order = Mage::getModel('sales/order')->load($orderId);
        }
        if ($this->_canViewOrder($order)) {
            if ($order->getStoreId()) {
                Mage::app()->setCurrentStore($order->getStoreId());
            }

            if (! isset($shipment) ) {
                $shipmentIds = array();
                foreach ($order->getShipmentsCollection() as $_shipment) {
                    $shipmentIds[] = $_shipment->getId();
                }
                $shipments = Mage::getResourceModel('sales/order_shipment_collection')
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('entity_id', array('in' => $shipmentIds))
                    ->load();
                if (!isset($pdf)){
                    $pdf = Mage::getModel('sales/order_pdf_shipment')->getPdf($shipments);
                } else {
                    $pages = Mage::getModel('sales/order_pdf_shipment')->getPdf($shipments);
                    $pdf->pages = array_merge ($pdf->pages, $pages->pages);
                }
            } else {
                $pdf = Mage::getModel('sales/order_pdf_shipment')->getPdf(array($shipment));
            }

            $this->_prepareDownloadResponse('shipment'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(), 'application/pdf');
        } else {
            $this->_redirect('*/*/history');
        }
    }

    public function printCreditmemoAction()
    {
        $creditmemoId = (int) $this->getRequest()->getParam('creditmemo_id');
        if ($creditmemoId) {
            $creditmemo = Mage::getModel('sales/order_creditmemo')->load($creditmemoId);
            $order = $creditmemo->getOrder();
        } else {
            $orderId = (int) $this->getRequest()->getParam('order_id');
            $order = Mage::getModel('sales/order')->load($orderId);
        }

        if ($this->_canViewOrder($order)) {
            if ($order->getStoreId()) {
                Mage::app()->setCurrentStore($order->getStoreId());
            }

            if (! isset($creditmemo) ) {
                $creditmemoIds = array();
                foreach ($order->getCreditmemosCollection() as $_creditmemo) {
                    $creditmemoIds[] = $_creditmemo->getId();
                }
                $creditmemos = Mage::getResourceModel('sales/order_creditmemo_collection')
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('entity_id', array('in' => $creditmemoIds))
                    ->load();
                if (!isset($pdf)){
                    $pdf =
Mage::getModel('sales/order_pdf_creditmemo')->getPdf($creditmemos);
                } else {
                    $pages =
Mage::getModel('sales/order_pdf_creditmemo')->getPdf($creditmemos);
                    $pdf->pages = array_merge ($pdf->pages, $pages->pages);
                }
            } else {
                $pdf = Mage::getModel('sales/order_pdf_creditmemo')->getPdf(array($creditmemo));
            }

            $this->_prepareDownloadResponse('creditmemo'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $pdf->render(), 'application/pdf');
        } else {
            $this->_redirect('*/*/history');
        }

    }


    protected function _prepareDownloadResponse($fileName, $content, $contentType =
'application/octet-stream', $contentLength = null)
    {
        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0',
true)
            ->setHeader('Content-type', $contentType, true)
            ->setHeader('Content-Length', is_null($contentLength) ? strlen($content) :
$contentLength)
            ->setHeader('Content-Disposition', 'attachment; filename=' . $fileName)
            ->setHeader('Last-Modified', date('r'));
        if (!is_null($content)) {
            $this->getResponse()->setBody($content);
        }
        return $this;
    }

}


/* vim: set ts=4 sw=4 expandtab nu tw=90: */

