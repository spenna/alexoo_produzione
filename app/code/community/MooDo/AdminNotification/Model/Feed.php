<?php
class MooDo_AdminNotification_Model_Feed extends Mage_AdminNotification_Model_Feed
{
    const XML_PATH_SETTING_FEED_URL = 'moodo_adminnotification/settings/feedurl';

    public function getFeedUrl()
    {
        if (is_null($this->_feedUrl)) {
            $this->_feedUrl = (Mage::getStoreConfigFlag(self::XML_USE_HTTPS_PATH) ? 'https://' : 'http://')
            . Mage::getStoreConfig(self::XML_PATH_SETTING_FEED_URL);
        }
        return $this->_feedUrl;
    }
    
    public function observe() {
       $model  = Mage::getModel('moodo_adminnotification/feed');
       $model->checkUpdate();
    }

    public function getLastUpdate()
        {
	        return Mage::app()->loadCache('moodo_adminnotification_notifications_lastcheck');
    }

    public function setLastUpdate()
    {
        Mage::app()->saveCache(time(), 'moodo_adminnotification_notifications_lastcheck');
         return $this;
    }

}
