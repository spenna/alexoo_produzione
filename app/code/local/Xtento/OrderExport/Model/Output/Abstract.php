<?php

/**
 * Product:       Xtento_OrderExport (1.2.0)
 * ID:            e6WLBQFx2/gx2klXgg3RFg49GuD6dk/ywJPoWrN/zYE=
 * Packaged:      2013-04-23T16:34:14+00:00
 * Last Modified: 2013-04-16T22:13:13+02:00
 * File:          app/code/local/Xtento/OrderExport/Model/Output/Abstract.php
 * Copyright:     Copyright (c) 2013 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

abstract class Xtento_OrderExport_Model_Output_Abstract extends Mage_Core_Model_Abstract implements Xtento_OrderExport_Model_Output_Interface
{
    static $iteratingKeys = array(
        'items',
        'transactions',
        'entries',
        'fields',
        'custom_options',
        'product_attributes',
        'product_options',
        'tracks',
        'order_status_history' => 'entry',
        'addresses' => 'address',
        'invoice_comments' => 'invoice_comment',
        'skus' => 'sku',
        'salesrules' => 'salesrule'
    );

    protected function _replaceFilenameVariables($filename, $exportArray)
    {
        $filename = str_replace("|", "-", $filename); // Remove the pipe character - it's not allowed in file names anyways and we use it to separate multiple files in the DB
        // Replace variables in filename
        $replaceableVariables = array(
            '/%d%/' => Mage::getSingleton('core/date')->gmtDate('d'),
            '/%m%/' => Mage::getSingleton('core/date')->gmtDate('m'),
            '/%y%/' => Mage::getSingleton('core/date')->gmtDate('y'),
            '/%Y%/' => Mage::getSingleton('core/date')->gmtDate('Y'),
            '/%h%/' => Mage::getSingleton('core/date')->gmtDate('H'),
            '/%i%/' => Mage::getSingleton('core/date')->gmtDate('i'),
            '/%s%/' => Mage::getSingleton('core/date')->gmtDate('s'),
            '/%lastentityid%/' => $this->getVariableValue('last_entity_id', $exportArray),
            '/%orderid%/' => $this->getVariableValue('last_entity_id', $exportArray), // Legacy
            '/%lastincrementid%/' => $this->getVariableValue('last_increment_id', $exportArray),
            '/%lastorderincrementid%/' => $this->getVariableValue('last_order_increment_id', $exportArray),
            '/%realorderid%/' => $this->getVariableValue('last_increment_id', $exportArray), // Legacy
            '/%ordercount%/' => $this->getVariableValue('collection_count', $exportArray), // Legacy
            '/%collectioncount%/' => $this->getVariableValue('collection_count', $exportArray),
            '/%uuid%/' => uniqid(),
            '/%exportid%/' => $this->getVariableValue('export_id', $exportArray),
        );
        Mage::unregister('last_exported_increment_id');
        Mage::register('last_exported_increment_id', $this->getVariableValue('last_increment_id', $exportArray));
        $filename = preg_replace(array_keys($replaceableVariables), array_values($replaceableVariables), $filename);
        return $filename;
    }

    protected function getVariableValue($variable, $exportArray)
    {
        $arrayToWorkWith = $exportArray;
        if ($variable == 'export_id') {
            if (Mage::registry('export_log')) {
                return Mage::registry('export_log')->getId();
            } else {
                return 0;
            }
        }
        if ($variable == 'collection_count') {
            return count($arrayToWorkWith);
        }
        if ($variable == 'total_item_count') {
            $totalItemCount = 0;
            foreach ($arrayToWorkWith as $collectionObject) {
                if (isset($collectionObject['items'])) {
                    foreach ($collectionObject['items'] as $item) {
                        $totalItemCount++;
                    }
                }
            }
            return $totalItemCount;
        }
        if ($variable == 'last_entity_id') {
            $lastItem = array_pop($arrayToWorkWith);
            if (isset($lastItem['entity_id'])) {
                return $lastItem['entity_id'];
            }
        }
        if ($variable == 'last_increment_id') {
            $lastItem = array_pop($arrayToWorkWith);
            if (isset($lastItem['increment_id'])) {
                return $lastItem['increment_id'];
            } else {
                return 'increment_not_set_' . $lastItem['entity_id'];
            }
        }
        if ($variable == 'last_order_increment_id') {
            $lastItem = array_pop($arrayToWorkWith);
            if (isset($lastItem['order']) && isset($lastItem['order']['increment_id'])) {
                return $lastItem['order']['increment_id'];
            } else {
                return $lastItem['increment_id'];
            }
        }
        if ($variable == 'date_from_timestamp') {
            $firstObject = array_shift($arrayToWorkWith);
            return Mage::helper('xtento_orderexport/date')->convertDateToStoreTimestamp($firstObject['created_at']);
        }
        if ($variable == 'date_to_timestamp') {
            $lastObject = array_pop($arrayToWorkWith);
            return Mage::helper('xtento_orderexport/date')->convertDateToStoreTimestamp($lastObject['created_at']);
        }
        return '';
    }

    protected function _throwXmlException($message)
    {
        $message .= "\n";
        foreach (libxml_get_errors() as $error) {
            $message .= "\tLine " . $error->line . ": " . $error->message;
            if (strpos($error->message, "\n") === FALSE) {
                $message .= "\n";
            }
        }
        libxml_clear_errors();
        Mage::throwException($message);
    }

    protected function _changeEncoding($input, $encoding)
    {
        $output = $input;
        if (!empty($encoding) && @function_exists('iconv')) {
            $output = @iconv("UTF-8", $encoding, $input);
            if (!$output) {
                // Error
            }
        }
        return $output;
    }
}