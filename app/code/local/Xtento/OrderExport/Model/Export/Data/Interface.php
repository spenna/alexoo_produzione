<?php

/**
 * Product:       Xtento_OrderExport (1.2.0)
 * ID:            e6WLBQFx2/gx2klXgg3RFg49GuD6dk/ywJPoWrN/zYE=
 * Packaged:      2013-04-23T16:34:14+00:00
 * Last Modified: 2012-11-18T20:56:11+01:00
 * File:          app/code/local/Xtento/OrderExport/Model/Export/Data/Interface.php
 * Copyright:     Copyright (c) 2013 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

interface Xtento_OrderExport_Model_Export_Data_Interface {
    public function getExportData($entityType, $collectionItem);
    public function getConfiguration();
}