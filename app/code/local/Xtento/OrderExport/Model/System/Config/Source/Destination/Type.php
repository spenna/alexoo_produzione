<?php

/**
 * Product:       Xtento_OrderExport (1.2.0)
 * ID:            e6WLBQFx2/gx2klXgg3RFg49GuD6dk/ywJPoWrN/zYE=
 * Packaged:      2013-04-23T16:34:14+00:00
 * Last Modified: 2013-02-10T16:57:50+01:00
 * File:          app/code/local/Xtento/OrderExport/Model/System/Config/Source/Destination/Type.php
 * Copyright:     Copyright (c) 2013 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_OrderExport_Model_System_Config_Source_Destination_Type
{
    public function toOptionArray()
    {
        return Mage::getSingleton('xtento_orderexport/destination')->getTypes();
    }

    public function getName($type) {
        foreach ($this->toOptionArray() as $optionType => $name) {
            if ($optionType == $type) {
                return $name;
            }
        }
        return '';
    }
}