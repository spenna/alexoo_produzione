<?php 

//require_once 'Mage/Catalog/Block/Navigation.php';

class Clickshop_Navigation_Block_Catalog_Navigation extends Mage_Catalog_Block_Navigation
{
  
  protected function _renderCategoryMenuItemHtmlXXXX($category, $level = 0, $isLast = false, $isFirst = false,
      $isOutermost = false, $outermostItemClass = '', $childrenWrapClass = '', $noEventAttributes = false)
  {
      if (!$category->getIsActive()) {
          return '';
      }
      $html = array();

      // get all children
      if (Mage::helper('catalog/category_flat')->isEnabled()) {
          $children = (array)$category->getChildrenNodes();
          $childrenCount = count($children);
      } else {
          $children = $category->getChildren();
          $childrenCount = $children->count();
      }
      $hasChildren = ($children && $childrenCount);

      // select active children
      $activeChildren = array();
      foreach ($children as $child) {
          if ($child->getIsActive()) {
              $activeChildren[] = $child;
          }
      }
      $activeChildrenCount = count($activeChildren);
      $hasActiveChildren = ($activeChildrenCount > 0);

      // prepare list item html classes
      $classes = array();
      $classes[] = 'level' . $level;
      $classes[] = 'nav-' . $this->_getItemPosition($level);
      if ($this->isCategoryActive($category)) {
          $classes[] = 'active';
      }
      $linkClass = '';
      if ($isOutermost && $outermostItemClass) {
          $classes[] = $outermostItemClass;
          $linkClass = ' class="'.$outermostItemClass.'"';
      }
      if ($isFirst) {
          $classes[] = 'first';
      }
      if ($isLast) {
          $classes[] = 'last';
      }
      if ($hasActiveChildren) {
          $classes[] = 'parent';
      }

      // prepare list item attributes
      $attributes = array();
      if (count($classes) > 0) {
          $attributes['class'] = implode(' ', $classes);
      }
      
      if ($hasActiveChildren && !$noEventAttributes) {

      }
      
      $attributes['onmouseover'] = 'toggleMenu(this,1)';
      $attributes['onmouseout'] = 'toggleMenu(this,0)';

      // assemble list item with attributes
      $htmlLi = '<li';
      foreach ($attributes as $attrName => $attrValue) {
          $htmlLi .= ' ' . $attrName . '="' . str_replace('"', '\"', $attrValue) . '"';
      }
      $htmlLi .= '>';
      $html[] = $htmlLi;

      $html[] = '<a href="'.$this->getCategoryUrl($category).'"'.$linkClass.'>';
      $html[] = '<span>' . $this->escapeHtml($category->getName()) . '</span>';
      $html[] = '</a>';

      // render children
      $htmlChildren = '';
      // $j = 0;
      // foreach ($activeChildren as $child) {
      //     $htmlChildren .= $this->_renderCategoryMenuItemHtml(
      //         $child,
      //         ($level + 1),
      //         ($j == $activeChildrenCount - 1),
      //         ($j == 0),
      //         false,
      //         $outermostItemClass,
      //         $childrenWrapClass,
      //         $noEventAttributes
      //     );
      //     $j++;
      // }
      
      $_category = Mage::getModel('catalog/category')->load( $category->getId() );
      $sizes = $this->getSizes($_category);
      $htmlSizes = '';
      
      if ( count($sizes) > 0 ) {
        $htmlSizes .= '<p class="sizes">';
        foreach ( $sizes as $size ) {
          $htmlSizes .= '<a href="'.$_category->getUrl().'?size='.$size['value'].'">'.$size['label'].'</a>';
        }
        $htmlSizes .= "</p>";
      }
      
      if ( $_category->getThumbnail() ) {
        $htmlChildren = '<p class="image"><img src="/media/catalog/category/'.$_category->getThumbnail().'" /></p><p class="description">'.$_category->getMetaTitle().'</p><h2>'.$this->__('Seleziona il tuo numero')."</h2>".$htmlSizes;
        
        
        if (!empty($htmlChildren)) {
            $html[] = '<div class="' . $childrenWrapClass . '">';
            $html[] = $htmlChildren;            
            $html[] = '</div>';
        }
      }
      
      unset($_category);
      unset($htmlChildren);

      
      $html[] = '</li>';

      $html = implode("\n", $html);
      return $html;
  }
  
  
  public function getSizes( $category ) {
    $sizes = array();
    $result = array();
    
    // $attribute_id = $this->getAttributeEAV_id('size');
    // $options = $this->getOptionArray( $attribute_id );
    $collection = Mage::getResourceModel('catalog/product_collection');
    $collection->addCategoryFilter($category);
    $collection->addAttributeToSelect('size');
    $collection->addAttributeToSort('size_value','asc');
    //$collection->addAttributeToSelect('type_id');
    $collection->addAttributeToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_SIMPLE );
    
    foreach ( $collection as $product ) {
      if (!in_array($product->getSize(),$sizes)) {
        $result[] = array( 'value' => $product->getSize(), 'label' => $product->getSizeValue() );
        $sizes[] = $product->getSize();
      }
    }
    
    // $sizes = array_unique( $sizes );
    // $result = array();
    // foreach ($sizes as $size) {
    //   $result[] = array('value' => $size, 'label' => $size_ );
    // }
    
    return $result;
  }
  
  
  /**
   * Retrieve entity_id given entity name of an attribute
   *
   * @author Andrea Restello
   * @return int
   */
  public function getAttributeEAV_id ( $attribute_name )
  {
    $connectipn = $this->getConnection();
    $result = $connectipn->query("SELECT attribute_id FROM eav_attribute WHERE entity_type_id = 4 AND attribute_code = '$attribute_name'")
                         ->fetchObject()
                         ->attribute_id;
    return $result;
  }
  
  public function getOptionArray( $attribute_id  ) {
    
    $attribute_collection = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $attribute_id );
		$attrubuteArray = array();
		foreach ( $attribute_collection->getSource()->getAllOptions(true, true) as $option) {
			$attrubuteArray[$option['value']] = $option['label'];
		}
		return $attrubuteArray;
  }
  
  public function getConnection() {
    return Mage::getSingleton('core/resource')->getConnection('core_read');
  }
  
}
 ?>