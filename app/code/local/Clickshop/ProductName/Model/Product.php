<?php
/**
* 
*/
class Clickshop_ProductName_Model_Product extends Mage_Catalog_Model_Product
{
  
  /**
   * Get product name
   *
   * @return string
   */
  public function getName()
  {
      $routes = array('catalogsearch','catalog');
      $route = Mage::app()->getRequest()->getRouteName();
      $name = $this->_getData('name');
      if ( $this->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_SIMPLE && in_array( $route, $routes ) ) {
        $parts = explode('-',$name);
        return $parts[0];
      }
      return $name;
  }
  
  
  /**
   * Retrieve sku through type instance
   *
   * @return string
   */
  public function getSku()
  {
      $routes = array('catalogsearch','catalog');
      $route = Mage::app()->getRequest()->getRouteName();
      $sku = $this->getTypeInstance(true)->getSku($this);
      if ( $this->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_SIMPLE && in_array( $route, $routes ) ) {
        $parts = explode('-',$sku);
        return $parts[0];
      }
      return $sku;
  }
  
  
}
