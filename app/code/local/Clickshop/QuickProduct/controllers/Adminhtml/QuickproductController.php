<?php

class Clickshop_QuickProduct_Adminhtml_QuickProductController extends Mage_Adminhtml_Controller_Action
{
  
  protected $images;
  static $configurable_attribute = array('size');
  
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('quickproduct/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('quickproduct/quickproduct')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('quickproduct_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('quickproduct/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('quickproduct/adminhtml_quickproduct_edit'))
				->_addLeft($this->getLayout()->createBlock('quickproduct/adminhtml_quickproduct_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('quickproduct')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
  	public function postAction() {
  	  $path = Mage::getBaseDir('media') . DS .'import' . DS;
		
  		if ($data = $this->getRequest()->getPost()) {
  		  
        // echo "<pre>";
        // print_r($data);
        // echo "</pre>";
        // die();
			
  			if (isset($_FILES['image']['name'])) {
  			  $filename = $_FILES['image']['name'];
  			  $temp = $_FILES['image']['tmp_name'];
  			  if ( is_uploaded_file( $temp ) ) 
  			  {
  			    move_uploaded_file( $temp, $path.$filename );
    				$filename = $path.$filename;
    				$this->image = $filename;
  			  }
  			}
  			
  			$associated = array();
        foreach ($data['size'] as $size_id ) {
          $associated[] = $this->addSimpleProduct( $data, $size_id );
        }
  			
  			$this->addConfigurableProduct( $data, $associated );  		  
	  			
  			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Product was successfully saved'));
      }
    
      $this->_redirect('*/*/');

  	}


    public function addSimpleProduct( $data, $size )
  	{
  	  $sku = $this->getSimpleSku($data,$size);
  	  $qty = $data['qty'][$size];
  	  $shelf = $data['shelf'];
  	  
  	  $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
  	  
      if ( is_object($product) && $product->getId() ) {
      } else {
        $product = new Mage_Catalog_Model_Product();
        $new = true;
        $product->setCreatedAt(strtotime('now'));
      }
  	  
      $product->setTypeId('simple'); 
	    $product->setSize( $size );
	    $product->setColor( $color );   
	    $product->setPrice( $this->getPrice($data) ); # Set some price
      $product->setSku( $sku );
			$product->setName( $data['name'] );
			$product->setDescription( $data['description'] );
			$product->setShortDescription( $data['description'] );    	  
			$product->setMulticolor( $data['color'] );
			$product->setMaterial( $data['material'] );
			$product->setManufacturer( $data['manufacturer'] );
			$product->setWearability( $data['wearability'] );
			$product->setPlateau( $data['plateau'] );
			$product->setHeel( $data['heel'] );
			$product->setShelf( $shelf );
			$product->setCategoryIds( array($data['category_id']) );
  	  $product->setVisibility( Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE );
  	  $product->setAttributeSetId( 4 );# 9 is for default 
  	  $product->setStoreId( 1 );
      $product->setWebsiteIDs( array( 1 ) ); # Website id, 1 is default
      $product->setStatus(1); 
      $product->setTaxClassId(2); # default tax class
      
      // $product = $this->addPhoto( $product );
      // $product = $this->addExtraPhoto( $data, $product );

      if ($new) {
        $product->setStockData(array( 
        'is_in_stock' => $qty > 0 ? 1 : 0, 
        'qty' => $qty
        ));
      } 
      
      try {
          $product-> setIsMassupdate( true );
      		$product-> setExcludeUrlRewrite( true );
          $product->save();
          return $product->getId();
      } 
      catch (Exception $e) { 
  		  Mage::getSingleton('adminhtml/session')->addError( (string)$e );
      }
      
      return null;
    }
    
    public function addConfigurableProduct( $data, $associated ) 
    {
      $new = false;
      $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $data['sku'] );
      if ( is_object($product) && $product->getId() ) {
      } else {
        $product = new Mage_Catalog_Model_Product();
        $product->setCreatedAt(strtotime('now'));
        $new = true;
      }
      
      Mage::register('current_product', $product );
  	  
      $product->setTypeId('configurable'); 
      $product->setName( $data['name']);
      $product->setSku( $data['sku']);
      $product->setPrice( $this->getPrice($data) );
      $product->setDescription( $data['description'] );
			$product->setShortDescription( $data['description'] );    	  
			$product->setMulticolor( $data['color'] );
      $product->setCategoryIds( array($data['category_id']) );
  	  $product->setAttributeSetId( 4 );# 9 is for default 
  	  $product->setStoreId( 1 );
      $product->setWebsiteIDs( array( 1 ) ); # Website id, 1 is default
      $product->setStatus(1); 
      $product->setTaxClassId(2); # default tax class

      $product->setMulticolor( $data['color'] );
			$product->setMaterial( $data['material'] );
			$product->setManufacturer( $data['manufacturer'] );
			$product->setWearability( $data['wearability'] );
			$product->setPlateau( $data['plateau'] );
			$product->setHeel( $data['heel'] );
			$product->setShelf( $data['shelf'] );
      
      $product->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH); //
      
      $this->addPhoto( $data );
      $this->addExtraPhoto( $data );
			
      if ($new) {
        $product->setStockData(array( 
        'is_in_stock' => 1
        ));
      }
      
  		$product->setCanSaveConfigurableAttributes(true);
			$configAttributeCodes = self::$configurable_attribute;
			$usingAttributeIds = array();
			foreach($configAttributeCodes as $attributeCode) {
				$attribute = $product->getResource()->getAttribute($attributeCode);
				if ($product->getTypeInstance()->canUseAttribute($attribute)) {
					if ($new) { // fix for duplicating attributes error
						$usingAttributeIds[] = $attribute->getAttributeId();
					}
				}
			}
			if ( !empty( $usingAttributeIds ) ) {
			    $product -> getTypeInstance()->setUsedProductAttributeIds( $usingAttributeIds );
			    $attributes_array = $product->getTypeInstance()->getConfigurableAttributesAsArray();
			    foreach($attributes_array as $key => $attribute_value) {
			        $attributes_array[$key]['label'] = $attribute_value['frontend_label'];
			    }
			    $product->setConfigurableAttributesData($attributes_array);
			    $product->setCanSaveConfigurableAttributes( true );
			    $product->setCanSaveCustomOptions( true );
			}
			
  		try {
  		  $product->save();
  		  $this->setAssociated( $product->getId(), $associated );
  			
  		} catch (Exception $e) {
  		  Mage::getSingleton('adminhtml/session')->addError( (string)$e );
  		}
  		
      Mage::unregister('current_product');
  		
    }
    
    public function getPrice( $data ) {
      return (float) str_replace( ',','.',$data['price'] );
    }
    
    public function getSimpleSku($data,$size) {
      $helper = Mage::helper('quickproduct');
      return $data['sku'].'-'.$helper->getOptionLabelById('size',$size);
    }
    
    public function addPhoto( $data ) {
      $product = Mage::registry('current_product');
      
			$base_dir = Mage::getBaseDir('media') . DS . 'import'. DS;
		  if ( file_exists($this->image) ) {
		    $product->addImageToMediaGallery( $this->image, array('thumbnail','small_image','image'), false, false);         /* exclude or not, default is true */
		  }
		}
		
		public function addExtraPhoto( $data ) {
		  $product = Mage::registry('current_product');
      
			$base_dir = Mage::getBaseDir('media') . DS . 'import'. DS;
			$names = trim( $data['images_extra'],';');
      $filenames = explode(';', $names );

      foreach ( $filenames as $filename ) {
        $filename = trim($filename);
        $filename = trim($filename,';');
        if ( file_exists($base_dir.$filename) && is_file($base_dir.$filename) ) {
          $product->addImageToMediaGallery( $base_dir.$filename, null, false, false);         /* exclude or not, default is true */
        }
      }
		}
		
		
		protected function setAssociated( $parent_id, $ids ) {

  	  $connection = Mage::getSingleton('core/resource')->getConnection('core_write');

  	  foreach ($ids as $child_id) {

  	    $exists = $connection->query("SELECT COUNT(child_id) as cnt FROM catalog_product_relation WHERE parent_id = '$parent_id' AND child_id = '$child_id' LIMIT 1");
  	    $find_product = (($exists->fetchObject()->cnt) > 0) ? true : false;
      	if (!$find_product) {
      	  $connection->query("INSERT INTO catalog_product_relation ( parent_id, child_id ) VALUES ( '$parent_id', '$child_id' )");
      	}

      	$exists = $connection->query("SELECT COUNT(parent_id) as cnt FROM catalog_product_super_link WHERE parent_id = '$parent_id' AND product_id = '$child_id' LIMIT 1");
  	    $find_product = (($exists->fetchObject()->cnt) > 0) ? true : false;
      	if (!$find_product) {
      	  $connection->query("INSERT INTO catalog_product_super_link ( parent_id, product_id ) VALUES ( '$parent_id', '$child_id' )");
      	}

  	  }


  	}
}
