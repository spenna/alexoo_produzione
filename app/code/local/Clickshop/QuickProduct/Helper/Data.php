<?php

class Clickshop_QuickProduct_Helper_Data extends Mage_Core_Helper_Abstract
{
  
	public function getAttributeValuesByName( $name ) {
    $attribute_id = $this->getAttributeEAV_id( $name );
    return $this->getAttributeValues( $attribute_id ); 
  }
  
  public function getAttributeEAV_id ( $attribute_name)
  {
	  $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
    $result = $connection->query("SELECT attribute_id FROM eav_attribute WHERE entity_type_id = 4 AND attribute_code = '$attribute_name'")
                         ->fetchObject()
                         ->attribute_id;
    return $result;
  }
  
  
  /**
   * Retrieve all values of a specific dropdown attribute 
   *
   * @param string $attribute_id 
   * @return array
   * @author Andrea Restello
   */
  public function getAttributeValues( $attribute_id ) {
    $attribute_collection = Mage::getModel('eav/config')->getAttribute('catalog_product', $attribute_id );
		$attrubuteArray = array();
		foreach ( $attribute_collection->getSource()->getAllOptions(true, true) as $option) {
			$attrubuteArray[$option['value']] = $option['label'];
		}
		return $attrubuteArray;
  }
  
  public function getOptionLabelById( $attribute_name, $option_id ) {
    $values = $this->getAttributeValuesByName( $attribute_name );
    foreach ($values as $id => $label) {
      if ($id == $option_id) {
        return $label;
      }
    }
    return false;
  }
  
  
}
