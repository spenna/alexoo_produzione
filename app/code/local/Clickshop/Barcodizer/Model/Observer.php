<?php
/**
 * Observer model
 *
 * @category    Mageservice
 * @package     Mageservice_Varnish
 * @author      Toni Grigoriu <toni@tonigrigoriu.com>
 */
class Clickshop_Barcodizer_Model_Observer
{
		/**
     * @see Mage_Core_Model_Cache
     * 
     * @param Mage_Core_Model_Observer $observer 
     */
    public function onProductSave($observer)
    {   
        $_product = $observer->getProduct();

				if ( $_product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_SIMPLE && $_product->getBarcode() == "") {
					
					$entity_id = $_product->getId();
					if (empty($entity_id)) {
						$connection = Mage::getSingleton('core/resource')->getConnection('core_write');
					  $entity_id = $connection->query("SHOW TABLE STATUS LIKE 'catalog_product_entity'")->fetchObject()->Auto_increment;
					}
					$barcode12 = '8'.sprintf( '%06s', $entity_id ).sprintf('%03s',$_product->getColor() ).$_product->getResource()->getAttribute('size')->getFrontend()->getValue($_product);
					$_product->setBarcode( $this->get_ean_checkdigit($barcode12,true) );
				}
			
        return $this;
		}
		
		
		/**
		 * Sales Barcode generation
		 *
		 * @return void
		 * @author Andrea Restello
		 **/
		public function onOrderSave( $observer )
		{
		  #ALTER TABLE sales_flat_order ADD barcode VARCHAR(13) NULL
      $order = $observer->getOrder();
      if ($order->getBarcode() == "" ) {
        $barcode12 = '8'.sprintf( '%011s', $order->getIncrementId() );
        $order->setBarcode( $this->get_ean_checkdigit($barcode12,true) );
      }
			return $this;
		}
		
		function get_ean_checkdigit($ean12, $full){

    	$ean12 =(string)$ean12;
    	// 1. Sommo le posizioni dispari
    	$even_sum = $ean12{1} + $ean12{3} + $ean12{5} + $ean12{7} + $ean12{9} + $ean12{11};
    	// 2. le moltiplico x 3
    	$even_sum_three = $even_sum * 3;
    	// 3. Sommo le posizioni pari
    	$odd_sum = $ean12{0} + $ean12{2} + $ean12{4} + $ean12{6} + $ean12{8} + $ean12{10};
    	// 4. Sommo i parziali precedenti
    	$total_sum = $even_sum_three + $odd_sum;
    	// 5. Il check digit è il numero più piccolo sottomultiplo di 10
    	$next_ten = (ceil($total_sum/10))*10;
    	$check_digit = $next_ten - $total_sum;

    	if($full==true) { // Ritorna tutto l'ean
    		return $ean12.$check_digit;
    	}
    	else { // Ritorna solo il check-digit
    		return $check_digit;
    	}
    }
}