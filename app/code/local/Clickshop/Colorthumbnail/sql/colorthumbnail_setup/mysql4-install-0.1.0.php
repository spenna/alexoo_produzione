<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('colorthumbnail')};
CREATE TABLE {$this->getTable('colorthumbnail')} (
  `colorthumbnail_id` int(11) unsigned NOT NULL auto_increment,
  `attribute_option_id` int(11) unsigned NULL,
  `title` varchar(255) NOT NULL default '',
  `color` varchar(255) NOT NULL default '',
  `color2` varchar(255) NOT NULL default '',
  `filename` varchar(255) NOT NULL default '',
  PRIMARY KEY (`colorthumbnail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 
