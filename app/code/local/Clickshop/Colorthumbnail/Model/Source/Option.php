<?php

class Clickshop_Colorthumbnail_Model_Source_Option extends Varien_Object
{
  
  public static function getConnection()
  {

      $config = Mage::getConfig()->getResourceConnectionConfig('core_write');

      $dbConfig = array(
                        'host'      => $config->host,
                        'username'  => $config->username,
                        'password'  => $config->password,
                        'dbname'    => $config->dbname,
                        'driver_options'=> array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8')

      );

      $db_magento = Zend_Db::factory('Pdo_Mysql', $dbConfig);

      return $db_magento;
  }
  
  /**
   * Retrieve entity_id given entity name of an attribute
   *
   * @author Andrea Restello
   * @return int
   */
  public static function getAttributeEAV_id ( $attribute_name)
  {
    $db_magento = self::getConnection();
    $result = $db_magento->query("SELECT attribute_id FROM eav_attribute WHERE entity_type_id = 4 AND attribute_code = '$attribute_name'")
                         ->fetchObject()
                         ->attribute_id;
    return $result;
  }

  static public function getOptionArray( $attribute_id = null ) {
    
    if ($attribute_id == null ) {
        $attribute_id = self::getAttributeEAV_id( 'multicolor' );
    }
    $attribute_collection = Mage::getModel('eav/config')->getAttribute('catalog_product', $attribute_id );
		$attrubuteArray = array();
		foreach ( $attribute_collection->getSource()->getAllOptions(true, true) as $option) {
			$attrubuteArray[$option['value']] = $option['label'];
		}
		return $attrubuteArray;
  }
  
}
