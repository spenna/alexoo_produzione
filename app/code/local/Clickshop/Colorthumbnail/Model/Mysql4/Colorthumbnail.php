<?php

class Clickshop_Colorthumbnail_Model_Mysql4_Colorthumbnail extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the colorthumbnail_id refers to the key field in your database table.
        $this->_init('colorthumbnail/colorthumbnail', 'colorthumbnail_id');
    }
}
