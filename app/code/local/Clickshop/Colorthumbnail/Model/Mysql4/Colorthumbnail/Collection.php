<?php

class Clickshop_Colorthumbnail_Model_Mysql4_Colorthumbnail_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('colorthumbnail/colorthumbnail');
    }
}
