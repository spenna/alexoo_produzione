<?php

class Clickshop_Colorthumbnail_Model_Colorthumbnail extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('colorthumbnail/colorthumbnail');
    }
}
