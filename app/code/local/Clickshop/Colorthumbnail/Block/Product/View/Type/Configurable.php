<?php 

//require_once 'Mage/Catalog/Block/Product/View/Type/Configurable.php';

class Clickshop_Colorthumbnail_Block_Product_View_Type_Configurable extends Mage_Catalog_Block_Product_View_Type_Configurable
{
  
  public function getJsonConfig()
  {
    $config =  Mage::helper('core')->jsonDecode( parent::getJsonConfig() );
    
    // $colorImages = array();
    // $colorImages2 = array();
	
    // $collection = Mage::getSingleton('colorthumbnail/colorthumbnail')->getCollection();
    // foreach ($collection as $item) {
    //   $colorImages[ $item->getAttributeOptionId() ] = $item->getColor(); // $item->getFilename()
    //   $colorImages2[ $item->getAttributeOptionId() ] = $item->getColor2();
    // }
    
    $attribute_id = Clickshop_Colorthumbnail_Model_Source_Option::getAttributeEAV_id('size');
    $config['sizes'] = Clickshop_Colorthumbnail_Model_Source_Option::getOptionArray( $attribute_id );
    
    // $config['colorImages'] = $colorImages;
    // $config['colorImages2'] = $colorImages2; 
    //$config['allow_backorders'] = $this->getProduct()->getData('allow_backorders');
    //$allowed = $this->getAllowProducts();
    // foreach ($allowed as $allow) {
    //     $qty = (int)Mage::getModel('cataloginventory/stock_item')->loadByProduct($allow)->getQty();
    //     $config['inventory'][ $allow->getColor() ] [ $allow->getSize() ] =  $qty;
    //     $config['inventory2'][ $allow->getId() ] = $qty;
    // }    
    return Mage::helper('core')->jsonEncode($config);    
  }
  
}
 ?>