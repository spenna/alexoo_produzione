<?php
class Clickshop_Colorthumbnail_Block_Adminhtml_Colorthumbnail extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_colorthumbnail';
    $this->_blockGroup = 'colorthumbnail';
    $this->_headerText = Mage::helper('adminhtml')->__('Color');
    $this->_addButtonLabel = Mage::helper('adminhtml')->__('Aggiungi Colore');
    parent::__construct();
  }
}
