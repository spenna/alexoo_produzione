<?php /**
* 
*/
class Clickshop_Colorthumbnail_Block_Adminhtml_Renderer_Filename extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
  
  public function render(Varien_Object $row)
  {
    if ( $row->getFilename() ) {
      return '<img src="/media/colors/'.$row->getFilename().'"/>';
    }
  }
  
}
 ?>