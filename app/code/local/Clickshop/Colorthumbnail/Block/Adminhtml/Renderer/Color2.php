<?php /**
* 
*/
class Clickshop_Colorthumbnail_Block_Adminhtml_Renderer_Color2 extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
  
  public function render(Varien_Object $row)
  {
    if ( $row->getColor2() ) {
      return '<p style="border:1px solid #ddd; background-color:#'.str_replace('#','',$row->getColor2()).'; width:40px; height:40px">&nbsp;</p>';
    }
  }
  
}
 ?>