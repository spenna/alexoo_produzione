<?php

class Clickshop_Colorthumbnail_Block_Adminhtml_Colorthumbnail_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'colorthumbnail';
        $this->_controller = 'adminhtml_colorthumbnail';
        
        $this->_updateButton('save', 'label', Mage::helper('colorthumbnail')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('colorthumbnail')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('colorthumbnail_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'colorthumbnail_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'colorthumbnail_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('colorthumbnail_data') && Mage::registry('colorthumbnail_data')->getId() ) {
            return Mage::helper('colorthumbnail')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('colorthumbnail_data')->getTitle()));
        } else {
            return Mage::helper('colorthumbnail')->__('Add Item');
        }
    }
}
