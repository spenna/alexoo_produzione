<?php

class Clickshop_Colorthumbnail_Block_Adminhtml_Colorthumbnail_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('colorthumbnail_form', array('legend'=>Mage::helper('colorthumbnail')->__('Item information')));
     
      // $fieldset->addField('title', 'text', array(
      //     'label'     => Mage::helper('colorthumbnail')->__('Title'),
      //     'class'     => 'required-entry',
      //     'required'  => true,
      //     'name'      => 'title',
      // ));
      
      $fieldset->addField('attribute_option_id', 'select', array(
          'label'     => Mage::helper('banner')->__('Option'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'attribute_option_id',
          'values'    => Mage::getSingleton('colorthumbnail/source_option')->getOptionArray(),
      ));
      
      $fieldset->addField('color', 'text', array(
          'label'     => Mage::helper('colorthumbnail')->__('Color'),
          'class'     => 'color',
          'required'  => false,
          'name'      => 'color',
          'id'        => 'color'
      ));

	  $fieldset->addField('color2', 'text', array(
	      'label'     => Mage::helper('colorthumbnail')->__('Color2'),
	      'class'     => 'color {required:false}',
	      'required'  => false,
	      'name'      => 'color2',
	      'id'        => 'color2'
	  ));

      $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('colorthumbnail')->__('Image'),
          'required'  => false,
          'name'      => 'filename',
	    ));
		
      // $fieldset->addField('status', 'select', array(
      //     'label'     => Mage::helper('colorthumbnail')->__('Status'),
      //     'name'      => 'status',
      //     'values'    => array(
      //         array(
      //             'value'     => 1,
      //             'label'     => Mage::helper('colorthumbnail')->__('Enabled'),
      //         ),
      // 
      //         array(
      //             'value'     => 2,
      //             'label'     => Mage::helper('colorthumbnail')->__('Disabled'),
      //         ),
      //     ),
      // ));
     
      // $fieldset->addField('content', 'editor', array(
      //     'name'      => 'content',
      //     'label'     => Mage::helper('colorthumbnail')->__('Content'),
      //     'title'     => Mage::helper('colorthumbnail')->__('Content'),
      //     'style'     => 'width:700px; height:500px;',
      //     'wysiwyg'   => false,
      //     'required'  => true,
      // ));
     
      if ( Mage::getSingleton('adminhtml/session')->getColorthumbnailData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getColorthumbnailData());
          Mage::getSingleton('adminhtml/session')->setColorthumbnailData(null);
      } elseif ( Mage::registry('colorthumbnail_data') ) {
          $form->setValues(Mage::registry('colorthumbnail_data')->getData());
      }
      return parent::_prepareForm();
  }
}
