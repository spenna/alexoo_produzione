<?php

class Clickshop_Colorthumbnail_Block_Adminhtml_Colorthumbnail_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('colorthumbnailGrid');
      $this->setDefaultSort('colorthumbnail_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('colorthumbnail/colorthumbnail')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('colorthumbnail_id', array(
          'header'    => Mage::helper('colorthumbnail')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'colorthumbnail_id',
      ));
      
      
      $this->addColumn('attribute_option_id', array(
          'header'    => Mage::helper('colorthumbnail')->__('Attribute Option Id'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'attribute_option_id',
      ));

      $this->addColumn('title', array(
          'header'    => Mage::helper('adminhtml')->__('Title'),
          'align'     =>'right',
          'index'     => 'title',
          'renderer' => new Clickshop_Colorthumbnail_Block_Adminhtml_Renderer_Title(),
      ));

    $this->addColumn('color', array(
        'header'    => Mage::helper('adminhtml')->__('Color'),
        'align'     =>'right',
        'width'     => '80px',
        'index'     => 'color',
        'renderer' => new Clickshop_Colorthumbnail_Block_Adminhtml_Renderer_Color(),
    ));

	$this->addColumn('color2', array(
	    'header'    => Mage::helper('adminhtml')->__('Color'),
	    'align'     =>'right',
	    'width'     => '80px',
	    'index'     => 'color2',
	    'renderer' => new Clickshop_Colorthumbnail_Block_Adminhtml_Renderer_Color2(),
	));
    
     $this->addColumn('filename', array(
         'header'    => Mage::helper('adminhtml')->__('Image'),
         'align'     =>'center',
         'width'     => '80px',
         'index'     => 'filename',
         'renderer' => new Clickshop_Colorthumbnail_Block_Adminhtml_Renderer_Filename(),
     ));
    

	  
     $this->addColumn('action',
         array(
             'header'    =>  Mage::helper('colorthumbnail')->__('Action'),
             'width'     => '100',
             'type'      => 'action',
             'getter'    => 'getId',
             'actions'   => array(
                 array(
                     'caption'   => Mage::helper('colorthumbnail')->__('Edit'),
                     'url'       => array('base'=> '*/*/edit'),
                     'field'     => 'id'
                 )
             ),
             'filter'    => false,
             'sortable'  => false,
             'index'     => 'stores',
             'is_system' => true,
     ));
		
		//$this->addExportType('*/*/exportCsv', Mage::helper('colorthumbnail')->__('CSV'));
		//$this->addExportType('*/*/exportXml', Mage::helper('colorthumbnail')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('colorthumbnail_id');
        $this->getMassactionBlock()->setFormFieldName('colorthumbnail');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('colorthumbnail')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('colorthumbnail')->__('Are you sure?')
        ));

        // $statuses = Mage::getSingleton('colorthumbnail/status')->getOptionArray();
        // 
        // array_unshift($statuses, array('label'=>'', 'value'=>''));
        // $this->getMassactionBlock()->addItem('status', array(
        //      'label'=> Mage::helper('colorthumbnail')->__('Change status'),
        //      'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
        //      'additional' => array(
        //             'visibility' => array(
        //                  'name' => 'status',
        //                  'type' => 'select',
        //                  'class' => 'required-entry',
        //                  'label' => Mage::helper('colorthumbnail')->__('Status'),
        //                  'values' => $statuses
        //              )
        //      )
        // ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}
