<?php

class Clickshop_Colorthumbnail_Block_Adminhtml_Colorthumbnail_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('colorthumbnail_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('colorthumbnail')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('colorthumbnail')->__('Item Information'),
          'title'     => Mage::helper('colorthumbnail')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('colorthumbnail/adminhtml_colorthumbnail_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
  
  protected function _afterToHtml($html)
	{
    	return parent::_afterToHtml($html);//. $this->_appendHtml();
  }
  
  private function _appendHtml()
  {
  	$html="
  	  <div id=\"colorpicker\"></div>
  	  <style type=\"text/css\" media=\"screen\">
  	   #colorpicker { position:absolute; left:800px; top:200px }
  	  </style>
      <script type=\"text/javascript\" charset=\"utf-8\">
        jQuery(document).ready(function() {
            jQuery('#colorpicker').farbtastic({ callback: '#color', width: 150, setColor:'#ffffff' });
            //var f = jQuery.farbtastic('#colorpicker');
            //f.setColor( jQuery('#color').val() );
          });
      </script>
	";
  return $html;
  }
  
}
