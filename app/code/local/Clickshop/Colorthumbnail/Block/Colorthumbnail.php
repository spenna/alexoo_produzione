<?php
class Clickshop_Colorthumbnail_Block_Colorthumbnail extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getColorthumbnail()     
     { 
        if (!$this->hasData('colorthumbnail')) {
            $this->setData('colorthumbnail', Mage::registry('colorthumbnail'));
        }
        return $this->getData('colorthumbnail');
        
    }
}
