<?php

require_once 'Mage/Adminhtml/Block/Sales/Order/Grid.php';

class Clickshop_Ordercolor_Block_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
  
  public function getRowClass($row)
  {
      return 'storeclass_' . $row->getStoreId(). " barcode_".$this->getBarcode( $row );
  }
  
  public function getBarcode( $row ) {
    $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
	  return $connection->query("SELECT barcode FROM sales_flat_order WHERE increment_id = '".$row->getIncrementId()."'")->fetchObject()->barcode;
  }

}