<?php
class Clickshop_QuickProduct_Block_QuickProduct extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getQuickProduct()     
     { 
        if (!$this->hasData('quickproduct')) {
            $this->setData('quickproduct', Mage::registry('quickproduct'));
        }
        return $this->getData('quickproduct');
        
    }
}
