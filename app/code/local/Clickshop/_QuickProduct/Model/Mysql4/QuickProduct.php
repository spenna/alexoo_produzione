<?php

class Clickshop_QuickProduct_Model_Mysql4_QuickProduct extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the quickproduct_id refers to the key field in your database table.
        $this->_init('quickproduct/quickproduct', 'quickproduct_id');
    }
}
