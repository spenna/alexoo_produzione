<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('brandthumbnail')};
CREATE TABLE {$this->getTable('brandthumbnail')} (
  `brandthumbnail_id` int(11) unsigned NOT NULL auto_increment,
  `attribute_option_id` int(11) unsigned NULL,
  `title` varchar(255) NOT NULL default '',
  `filename` varchar(255) NOT NULL default '',
  PRIMARY KEY (`brandthumbnail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 
