<?php

class Clickshop_Brandthumbnail_Model_Brandthumbnail extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('brandthumbnail/brandthumbnail');
    }
}
