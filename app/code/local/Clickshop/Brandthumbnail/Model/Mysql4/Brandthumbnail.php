<?php

class Clickshop_Brandthumbnail_Model_Mysql4_Brandthumbnail extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the brandthumbnail_id refers to the key field in your database table.
        $this->_init('brandthumbnail/brandthumbnail', 'brandthumbnail_id');
    }
}
