<?php

class Clickshop_Brandthumbnail_Model_Mysql4_Brandthumbnail_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('brandthumbnail/brandthumbnail');
    }
}
