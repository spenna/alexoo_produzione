<?php
class Clickshop_Brandthumbnail_Block_Category extends Mage_Catalog_Block_Navigation
{
  
  static $manufacturer;
  
	public function _prepareLayout()
  {
    self::$manufacturer = $this->getManufactuers();
		return parent::_prepareLayout();
  }
    
  public function getBrands() {
    return Mage::getModel('brandthumbnail/brandthumbnail')->getCollection();
  }
  
  
  public function getManufactuers() {
    $values = $this->getAttributeValues( $this->getAttributeEAV_id('manufacturer') );
    return $values;
  }
  
  public function getBrandUrl( $brand_id ) {
    $label = self::$manufacturer[ $brand_id ];
		$url = Mage::getUrl('/').'marchi/'.$this->stripText( $label ).'.html';
    return $url;
  }
  
  /**
   * Retrieve all values of a specific dropdown attribute 
   *
   * @param string $attribute_id 
   * @return array
   * @author Andrea Restello
   */
  public function getAttributeValues( $attribute_id ) {
    $attribute_collection = Mage::getModel('eav/config')->getAttribute('catalog_product', $attribute_id );
		$attrubuteArray = array();
		foreach ( $attribute_collection->getSource()->getAllOptions(true, true) as $option) {
			$attrubuteArray[$option['value']] = $option['label'];
		}
		return $attrubuteArray;
  }
  
  
  public function getAttributeEAV_id ( $attribute_name)
  {
    $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
    $result = $connection->query("SELECT attribute_id FROM eav_attribute WHERE entity_type_id = 4 AND attribute_code = '$attribute_name'")
                         ->fetchObject()
                         ->attribute_id;
    return $result;
  }
  
  public function stripText($text)
  {
    $text = strtolower($text);
    // strip all non word chars
    $text = preg_replace('/\W/', ' ', $text);

    // replace all white space sections with a dash
    $text = preg_replace('/\ +/', '-', $text);

    // trim dashes
    $text = preg_replace('/\-$/', '', $text);
    $text = preg_replace('/^\-/', '', $text);

    return $text;
  }
}