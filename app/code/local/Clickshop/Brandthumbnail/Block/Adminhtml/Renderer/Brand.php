<?php /**
* 
*/
class Clickshop_Brandthumbnail_Block_Adminhtml_Renderer_Brand extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
  
  public function render(Varien_Object $row)
  {
    if ( $row->getBrand() ) {
      return '<p style="border:1px solid #ddd; background-brand:'.$row->getBrand().'; width:40px; height:40px">&nbsp;</p>';
    }
  }
  
}
 ?>