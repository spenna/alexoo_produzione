<?php /**
* 
*/
class Clickshop_Brandthumbnail_Block_Adminhtml_Renderer_Title extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
  
  public function render(Varien_Object $row)
  {
   if ($row->getAttributeOptionId()) {
     $options = Clickshop_Brandthumbnail_Model_Source_Option::getOptionArray();
     return $options[ $row->getAttributeOptionId() ];
     
   }
  }
}
 ?>