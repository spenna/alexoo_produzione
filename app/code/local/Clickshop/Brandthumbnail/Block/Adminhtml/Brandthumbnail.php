<?php
class Clickshop_Brandthumbnail_Block_Adminhtml_Brandthumbnail extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_brandthumbnail';
    $this->_blockGroup = 'brandthumbnail';
    $this->_headerText = Mage::helper('adminhtml')->__('Brand');
    $this->_addButtonLabel = Mage::helper('adminhtml')->__('Aggiungi Brand');
    parent::__construct();
  }
}
