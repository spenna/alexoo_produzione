<?php

class Clickshop_Brandthumbnail_Block_Adminhtml_Brandthumbnail_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('brandthumbnail_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('brandthumbnail')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('brandthumbnail')->__('Item Information'),
          'title'     => Mage::helper('brandthumbnail')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('brandthumbnail/adminhtml_brandthumbnail_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
  
  protected function _afterToHtml($html)
	{
    	return parent::_afterToHtml($html). $this->_appendHtml();
  }
  
  private function _appendHtml()
  {
  	$html="
  	  <div id=\"brandpicker\"></div>
  	  <style type=\"text/css\" media=\"screen\">
  	   #brandpicker { position:absolute; left:800px; top:200px }
  	  </style>
      <script type=\"text/javascript\" charset=\"utf-8\">
        jQuery(document).ready(function() {
            jQuery('#brandpicker').farbtastic({ callback: '#brand', width: 150, setBrand:'#ffffff' });
            //var f = jQuery.farbtastic('#brandpicker');
            //f.setBrand( jQuery('#brand').val() );
          });
      </script>
	";
  return $html;
  }
  
}
