<?php

class Clickshop_Brandthumbnail_Block_Adminhtml_Brandthumbnail_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('brandthumbnailGrid');
      $this->setDefaultSort('brandthumbnail_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('brandthumbnail/brandthumbnail')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('brandthumbnail_id', array(
          'header'    => Mage::helper('brandthumbnail')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'brandthumbnail_id',
      ));
      
      
      $this->addColumn('attribute_option_id', array(
          'header'    => Mage::helper('brandthumbnail')->__('Attribute Option Id'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'attribute_option_id',
      ));

      $this->addColumn('title', array(
          'header'    => Mage::helper('adminhtml')->__('Title'),
          'align'     =>'right',
          'index'     => 'title',
          'renderer' => new Clickshop_Brandthumbnail_Block_Adminhtml_Renderer_Title(),
      ));

    $this->addColumn('brand', array(
        'header'    => Mage::helper('adminhtml')->__('Brand'),
        'align'     =>'right',
        'width'     => '80px',
        'index'     => 'brand',
        'renderer' => new Clickshop_Brandthumbnail_Block_Adminhtml_Renderer_Brand(),
    ));
    
      $this->addColumn('filename', array(
          'header'    => Mage::helper('adminhtml')->__('Image'),
          'align'     =>'center',
          'width'     => '80px',
          'index'     => 'filename',
          'renderer' => new Clickshop_Brandthumbnail_Block_Adminhtml_Renderer_Filename(),
      ));
    

	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('brandthumbnail')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('brandthumbnail')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		//$this->addExportType('*/*/exportCsv', Mage::helper('brandthumbnail')->__('CSV'));
		//$this->addExportType('*/*/exportXml', Mage::helper('brandthumbnail')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('brandthumbnail_id');
        $this->getMassactionBlock()->setFormFieldName('brandthumbnail');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('brandthumbnail')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('brandthumbnail')->__('Are you sure?')
        ));

        // $statuses = Mage::getSingleton('brandthumbnail/status')->getOptionArray();
        // 
        // array_unshift($statuses, array('label'=>'', 'value'=>''));
        // $this->getMassactionBlock()->addItem('status', array(
        //      'label'=> Mage::helper('brandthumbnail')->__('Change status'),
        //      'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
        //      'additional' => array(
        //             'visibility' => array(
        //                  'name' => 'status',
        //                  'type' => 'select',
        //                  'class' => 'required-entry',
        //                  'label' => Mage::helper('brandthumbnail')->__('Status'),
        //                  'values' => $statuses
        //              )
        //      )
        // ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}
