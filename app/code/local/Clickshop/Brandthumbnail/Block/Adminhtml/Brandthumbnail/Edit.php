<?php

class Clickshop_Brandthumbnail_Block_Adminhtml_Brandthumbnail_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'brandthumbnail';
        $this->_controller = 'adminhtml_brandthumbnail';
        
        $this->_updateButton('save', 'label', Mage::helper('brandthumbnail')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('brandthumbnail')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('brandthumbnail_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'brandthumbnail_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'brandthumbnail_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('brandthumbnail_data') && Mage::registry('brandthumbnail_data')->getId() ) {
            return Mage::helper('brandthumbnail')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('brandthumbnail_data')->getTitle()));
        } else {
            return Mage::helper('brandthumbnail')->__('Add Item');
        }
    }
}
