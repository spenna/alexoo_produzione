<?php

class Clickshop_Brandthumbnail_Block_Adminhtml_Brandthumbnail_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('brandthumbnail_form', array('legend'=>Mage::helper('brandthumbnail')->__('Item information')));
     
      // $fieldset->addField('title', 'text', array(
      //     'label'     => Mage::helper('brandthumbnail')->__('Title'),
      //     'class'     => 'required-entry',
      //     'required'  => true,
      //     'name'      => 'title',
      // ));
      
      $fieldset->addField('attribute_option_id', 'select', array(
          'label'     => Mage::helper('banner')->__('Option'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'attribute_option_id',
          'values'    => Mage::getSingleton('brandthumbnail/source_option')->getOptionArray(),
      ));
      
      $fieldset->addField('brand', 'text', array(
          'label'     => Mage::helper('brandthumbnail')->__('Brand'),
          //'class'     => 'required-entry',
          'required'  => false,
          'name'      => 'brand',
          'id'        => 'brand'
      ));

      $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('brandthumbnail')->__('Image'),
          'required'  => false,
          'name'      => 'filename',
	    ));
		
      // $fieldset->addField('status', 'select', array(
      //     'label'     => Mage::helper('brandthumbnail')->__('Status'),
      //     'name'      => 'status',
      //     'values'    => array(
      //         array(
      //             'value'     => 1,
      //             'label'     => Mage::helper('brandthumbnail')->__('Enabled'),
      //         ),
      // 
      //         array(
      //             'value'     => 2,
      //             'label'     => Mage::helper('brandthumbnail')->__('Disabled'),
      //         ),
      //     ),
      // ));
     
      // $fieldset->addField('content', 'editor', array(
      //     'name'      => 'content',
      //     'label'     => Mage::helper('brandthumbnail')->__('Content'),
      //     'title'     => Mage::helper('brandthumbnail')->__('Content'),
      //     'style'     => 'width:700px; height:500px;',
      //     'wysiwyg'   => false,
      //     'required'  => true,
      // ));
     
      if ( Mage::getSingleton('adminhtml/session')->getBrandthumbnailData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getBrandthumbnailData());
          Mage::getSingleton('adminhtml/session')->setBrandthumbnailData(null);
      } elseif ( Mage::registry('brandthumbnail_data') ) {
          $form->setValues(Mage::registry('brandthumbnail_data')->getData());
      }
      return parent::_prepareForm();
  }
}
