<?php
class Clickshop_Banner_Block_Banner extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getBannersByTypeId( $type = 1)     
     {  
       
        $collection = Mage::getModel('banner/banner')->getCollection();
        $collection->addFieldToFilter('website_id', array('eq' => Mage::app()->getStore()->getWebsiteId() ) );
        $collection->addFieldToFilter('status', array('eq' => 1));
        $collection->addFieldToFilter('type', array('eq' => $type ));
        $collection->getSelect()->order( array('rank') );
        return $collection;
        
    }
    
    
    public function getItems() {
      return $this->getBannersByTypeId( $this->getTypeId() );
    }
}
