<?php

class Clickshop_Banner_Block_Adminhtml_Banner_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('bannerGrid');
      $this->setDefaultSort('banner_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('banner/banner')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('banner_id', array(
          'header'    => Mage::helper('banner')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'banner_id',
      ));
      
      $this->addColumn('type', array(
          'header'    => Mage::helper('banner')->__('Type'),
          'align'     =>'right',
          'width'     => '200px',
          'index'     => 'type',
          'renderer' => new Clickshop_Banner_Block_Adminhtml_Renderer_Type(),
      ));
      
      $websites = Mage::getModel('core/website')->getCollection()->toOptionHash();
      
      $this->addColumn('website_id', array(
          'header'    => Mage::helper('adminhtml')->__('Website'),
          'align'     => 'left',
          'width'     => '120px',
          'index'     => 'website_id',
          'type'      => 'options',
          'options'   => array_keys($websites),
          'renderer' => new Clickshop_Banner_Block_Adminhtml_Renderer_Website(),
          
      ));
      
      $this->addColumn('category', array(
          'header'    => Mage::helper('adminhtml')->__('Category'),
          'align'     =>'left',
          'index'     => 'category_id',
          'renderer' => new Clickshop_Banner_Block_Adminhtml_Renderer_Category(),
      ));

      $this->addColumn('title', array(
          'header'    => Mage::helper('banner')->__('Title'),
          'align'     =>'left',
          'index'     => 'title',
      ));
      
      $this->addColumn('rank', array(
          'header'    => Mage::helper('banner')->__('Posizione'),
          'align'     =>'right',
          'index'     => 'rank',
      ));

	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('banner')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */

      $this->addColumn('status', array(
          'header'    => Mage::helper('banner')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
      
      
      $this->addColumn('filename', array(
          'header'    => Mage::helper('banner')->__('Immagine'),
          'align'     =>'right',
          'width'     => '200px',
          'index'     => 'filename',
          'renderer' => new Clickshop_Banner_Block_Adminhtml_Renderer_Filename(),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('banner')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('banner')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
    // $this->addExportType('*/*/exportCsv', Mage::helper('banner')->__('CSV'));
    // $this->addExportType('*/*/exportXml', Mage::helper('banner')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('banner_id');
        $this->getMassactionBlock()->setFormFieldName('banner');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('banner')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('banner')->__('Are you sure?')
        ));
        
        $statuses = Mage::getSingleton('banner/status')->getOptionArray();
        
        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('banner')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('banner')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}
