<?php

class Clickshop_Banner_Block_Adminhtml_Banner_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('banner_form', array('legend'=>Mage::helper('banner')->__('Item information')));
     
      $fieldset->addField('type', 'select', array(
          'label'     => Mage::helper('banner')->__('Tipo'),
          'name'      => 'type',
          'values'    => Mage::getSingleton('banner/source_type')->getOptionArray(),
      ));
      
      $websites = Mage::getModel('core/website')->getCollection()->toOptionHash();
  		$fieldset->addField('website_id', 'select', array(
          'label'     => Mage::helper('adminhtml')->__('Website'),
          'name'      => 'website_id',
          'values'    => $websites,
      ));
      
      $fieldset->addField('category_id', 'select', array(
          'label'     => Mage::helper('banner')->__('Category'),
          //'class'     => 'required-entry',
          'required'  => false,
          'name'      => 'category_id',
          'values'    => Mage::getSingleton('banner/source_category')->getOptionArray(),
      ));
      
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('banner')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));

      $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('banner')->__('File'),
          'required'  => false,
          'name'      => 'filename',
	  ));
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('banner')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('banner')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('banner')->__('Disabled'),
              ),
          ),
      ));
      
      
      $fieldset->addField('link', 'text', array(
          'label'     => Mage::helper('banner')->__('Link'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'link',
      ));
    
    $ranks = array();
    for ($i=1; $i < 50; $i++) { 
      $ranks[] = array('value' => $i, 'label' => $i );
    }  
    $fieldset->addField('rank', 'select', array(
        'label'     => Mage::helper('banner')->__('Posizione'),
        'name'      => 'rank',
        'values'    => $ranks
    ));
     
      // $fieldset->addField('content', 'editor', array(
      //     'name'      => 'content',
      //     'label'     => Mage::helper('banner')->__('Content'),
      //     'title'     => Mage::helper('banner')->__('Content'),
      //     'style'     => 'width:700px; height:500px;',
      //     'wysiwyg'   => false,
      //     'required'  => true,
      // ));
     
      if ( Mage::getSingleton('adminhtml/session')->getBannerData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getBannerData());
          Mage::getSingleton('adminhtml/session')->setBannerData(null);
      } elseif ( Mage::registry('banner_data') ) {
          $form->setValues(Mage::registry('banner_data')->getData());
      }
      return parent::_prepareForm();
  }
}
