<?php
class Clickshop_Banner_Block_Adminhtml_Type extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_type';
    $this->_blockGroup = 'banner';
    $this->_headerText = Mage::helper('banner')->__('Gestione tipi banner');
    $this->_addButtonLabel = Mage::helper('banner')->__('Aggiungi tipo banner');
    parent::__construct();
  }
}
