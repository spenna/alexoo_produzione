<?php /**
* 
*/
class Clickshop_Banner_Block_Adminhtml_Renderer_Website extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
  
  public function render(Varien_Object $row)
  {
    $websites = Mage::getModel('core/website')->getCollection()->toOptionHash();
    //$websites = array_combine( array_values($websites), array_keys($websites)  );
    if (isset($websites[ $row->getWebsiteId() ])) {
      return $websites[ $row->getWebsiteId() ];
    } else {
      "Tutte i siti";
    }
  }
}
 ?>