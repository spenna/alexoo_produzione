<?php /**
* 
*/
class Clickshop_Banner_Block_Adminhtml_Renderer_Filename extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
  
  public function render(Varien_Object $row)
  {
    if ( $row->getFilename() ) {
      return '<img src="/media/banners/'.$row->getFilename().'" height="80"/>';
    }
  }
  
}
 ?>