<?php /**
* 
*/
class Clickshop_Banner_Block_Adminhtml_Renderer_Type extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
  
  public function render(Varien_Object $row)
  {
    $options = Clickshop_Banner_Model_Source_Type::getOptionArray();

    if ( $row->getType() ) {
      return $options[ $row->getType() ];
    }
  }
  
}
 ?>