<?php /**
* 
*/
class Clickshop_Banner_Block_Adminhtml_Renderer_Category extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
  
  public function render(Varien_Object $row)
  {
   if ($row->getCategoryId()) {
     $options = Clickshop_Banner_Model_Source_Category::getOptionArray();
     return $options[ $row->getCategoryId() ];
     
   }
  }
}
 ?>