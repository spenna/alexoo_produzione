<?php

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('banner')};
CREATE TABLE {$this->getTable('banner')} (
  `banner_id` int(11) unsigned NOT NULL auto_increment,
  `website_id` int(11) NULL,
  `category_id` int(11) NULL,
  `title` varchar(255) NOT NULL default '',
  `color` varchar(6) NULL default '',
  `filename` varchar(255) NOT NULL default '',
  `link` text NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `type` smallint(6) NULL,
  `rank` smallint(6) NOT NULL default '0',
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");


$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('type')};
CREATE TABLE {$this->getTable('type')} (
  `type_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup(); 
