<?php
/**
 * Clickshop_Banner
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0), a
 * copy of which is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Clickshop
 * @package    Clickshop_ImageCdn
 * @author     Clickshop Codemaster <codemaster@onepica.com>
 * @copyright  Copyright (c) 2009 One Pica, Inc.
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

class Clickshop_Banner_Model_Source_Type
{    
  
  static $_items;
  
  static function _getTypeCollection() {
    if (is_null(self::$_items)) {

        $collection = Mage::getModel('banner/type')->getCollection();

        self::$_items = $collection;

    }
    return self::$_items;
  }
  
    /**
	 * Gets the list of CDNs for the admin config dropdown. Allowing $types to
	 * be passed in enables third-paries to easily add adapters to the results
	 *
	 * @param array $types
	 * @return array
	 */
    static public function getOptionArray()
    {    	
      $items = self::_getTypeCollection();
      $_options = array("" => Mage::helper('banner')->__("Select") );
      foreach ($items as $item) {
        $_options[ $item->getTypeId() ] = $item->getTitle();
      }
      return  $_options;
    }
    
}
