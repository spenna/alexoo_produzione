<?php

class Clickshop_Banner_Model_Source_Category
{
  
  static public function getCategoryArrayOption()
  {
    $helper     = Mage::helper('catalog/category');
    $collection = $helper->getStoreCategories('name', true, false);
    $result = array();
    foreach ($collection as $category) {
      $result[] = array(
        'value' => $category->getId(),
        'label' => $category->getName()
      );
    }
    return $result;
  }

  static public function getOptionArray() {
    $collection = Mage::getModel('catalog/category')->getCollection()->addAttributeToSort('name', 'ASC');
    $collection->addAttributeToSelect('name');
    $result = array('' => '-- Scegli una --');
    foreach ($collection as $category) {
      $result[$category->getId()] = $category->getName();
    }
    return $result;
  }

}