<?php

class Clickshop_Pickinglist_Model_Mysql4_Pickinglist_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('pickinglist/pickinglist');
    }
}
