<?php

class Clickshop_Pickinglist_Model_Mysql4_Pickinglist extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the pickinglist_id refers to the key field in your database table.
        $this->_init('pickinglist/pickinglist', 'pickinglist_id');
    }
}
