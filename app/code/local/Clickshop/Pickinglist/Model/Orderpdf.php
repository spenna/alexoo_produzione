<?php

class Clickshop_Pickinglist_Model_Orderpdf extends Mage_Core_Model_Abstract
{
  public function _construct()
  {
      parent::_construct();
      $this->_init('pickinglist/orderpdf');
  }
  
  public function printOrders( $ordersIds ) {
    
    $orders = Mage::getModel('sales/order')
        ->getCollection()
        ->addAttributeToSelect('*')
        ->addAttributeToFilter('entity_id', array('in' => $ordersIds));
    
    $pdf = Mage::getModel('Nastnet_OrderPrint/order_pdf_order')->getPdf($orders);
    //$pdf = Mage::getModel('pickinglist/order_pdf_order')->getPdf($orders);
    return $pdf;
  }
  
}