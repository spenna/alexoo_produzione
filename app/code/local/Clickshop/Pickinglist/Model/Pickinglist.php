<?php

class Clickshop_Pickinglist_Model_Pickinglist extends Mage_Core_Model_Abstract
{
    protected $pdf;
    const MARGIN_TOP = 50;
    const MARGIN_BOTTOM = 50;
    const BLOCK_HEIGHT = 15;
    
    public function _construct()
    {
        parent::_construct();
        $this->_init('pickinglist/pickinglist');
    }
    
    public function exportPickinglist( $orders ) {
      
      $products = $this->countItems( $orders );
      $colors = $this->getAttributeValues( $this->getAttributeEAV_id('multicolor') );
			$sizes = $this->getAttributeValues( $this->getAttributeEAV_id('size') );
			
      $pdf = new Zend_Pdf();
      $font = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA);
      $font_size = 12;
      $x = 10;
      $page_num = 1;
      $row = 1;
      $i = 0;
      $total = count($products);
      
      foreach ( $products as $product ) {
        
        if ($row == 1) { 
          $page = $this->addHeader();
          $pageWidth = $page->getWidth();
          $pageHeight = $page->getHeight()-self::MARGIN_TOP;
          $y = $pageHeight - 25;
          $step_x = $pageWidth / 12;
        }
        
        $_product = $product['product'];
        $color = explode(',',$_product->getMulticolor());
        $page->setFont( $font, $font_size )->drawText( $_product->getSku(), $x+($step_x*0), $y );
        $page->setFont( $font, $font_size )->drawText( str_replace('pickinglist','',$_product->getName()), $x+($step_x*2), $y );
        $page->setFont( $font, $font_size )->drawText( $colors[ $color[0] ], $x+($step_x*6), $y );
        $page->setFont( $font, $font_size )->drawText( $sizes[ $_product->getSize() ], $x+($step_x*7), $y );
        $page->setFont( $font, $font_size )->drawText( $_product->getBarcode(), $x+($step_x*8), $y );
        $page->setFont( $font, $font_size )->drawText( (int)$product['qty'], $x+($step_x*10)-5, $y );
        $page->setFont( $font, $font_size )->drawText( $_product->getShelf(), $x+($step_x*11)-30, $y );
        
        $row++;
        $i++;
        $y -= self::BLOCK_HEIGHT;
        
        if ( $y < self::MARGIN_BOTTOM || $i == $total ) {
            $page_num++;
            $pdf->pages[] = $page;
            unset($page);
            $row = 1;
        }
        
      }
      
      $this->pdf = $pdf;
    }
     
     
    public function printPdf() {
      header("Content-Disposition: attachment; filename=pickinglist".date('Y-m-d').".pdf");
      header('Content-Type: application/pdf');
      echo $this->pdf->render();
      exit();
    }
     
    /**
     * Prepare an array of summed products ordered by shelf number
     *
     * @param array $orders 
     * @return array
     * @author Andrea Restello
     */
    public function countItems( array $orders ) {
      //Collect orders
      $collection = Mage::getModel('sales/order')->getCollection();
      $collection->addAttributeToFilter('entity_id', array(
        'in' => $orders 
      ) );
        
      foreach ( $collection as $order ) {
        
        foreach ( $order->getAllItems() as $item ) {
            $_product = Mage::getModel('catalog/product')->load( $item->getProductId() );
            
            if ( $_product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_SIMPLE ) {
              $shelf = $_product->getShelf().'-'.$_product->getId();
              if ( isset( $products[ $shelf ] ) ) {
                $products[ $shelf ][ 'qty' ] += $item->getQtyOrdered(); // just add present quantity
              } else {
                $products[ $shelf ] = array(
                  'qty' => $item->getQtyOrdered(),
                  'product' => $_product,
                );
              }
            }
        }
      }
      ksort( $products );
      return $products;
    }
    
    public function addHeader() {
      
      $helper = Mage::helper('pickinglist');
      
      $page = new Zend_Pdf_Page(Zend_Pdf_Page::SIZE_A4);
      $font_bold = Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD);
      $font_size = 12;

      $pageHeight = $page->getHeight()-self::MARGIN_TOP;
      $pageWidth = $page->getWidth();
      $x = 10;
      $step_x = $pageWidth / 12;
      
      $page->setFont( $font_bold, $font_size )->drawText( $helper->__('Picking List').' '.date('d-m-Y'), $x+($step_x*0), $pageHeight +20 );
      
      $page->setFont( $font_bold, $font_size+1 )->drawText( $helper->__('Sku'), $x+($step_x*0), $pageHeight );
      $page->setFont( $font_bold, $font_size+1 )->drawText( $helper->__('Name'), $x+($step_x*2), $pageHeight );
      $page->setFont( $font_bold, $font_size+1 )->drawText( $helper->__('Color'), $x+($step_x*6), $pageHeight );
      $page->setFont( $font_bold, $font_size+1 )->drawText( $helper->__('Taglia'), $x+($step_x*7), $pageHeight );
      $page->setFont( $font_bold, $font_size+1 )->drawText( $helper->__('Barcode'), $x+($step_x*8), $pageHeight );
      $page->setFont( $font_bold, $font_size+1 )->drawText( $helper->__('Qt'), $x+($step_x*10)-5, $pageHeight );
      $page->setFont( $font_bold, $font_size+1 )->drawText( $helper->__('Shelf'), $x+($step_x*11)-30, $pageHeight );
      
      $page->drawLine( $x, $pageHeight-10, $x + $pageWidth, $pageHeight-10 );
      
      return $page;
      
    }
    
    
    /**
     * Retrieve all values of a specific dropdown attribute 
     *
     * @param string $attribute_id 
     * @return array
     * @author Andrea Restello
     */
    public function getAttributeValues( $attribute_id ) {
      $attribute_collection = Mage::getModel('eav/config')->getAttribute('catalog_product', $attribute_id );
  		$attrubuteArray = array();
  		foreach ( $attribute_collection->getSource()->getAllOptions(true, true) as $option) {
  			$attrubuteArray[$option['value']] = $option['label'];
  		}
  		return $attrubuteArray;
    }
    
    /**
     * Retrieve entity_id given entity name of an attribute
     *
     * @author Andrea Restello
     * @return int
     */
    public function getAttributeEAV_id ( $attribute_name)
    {
      $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
      $result = $connection->query("SELECT attribute_id FROM eav_attribute WHERE entity_type_id = 4 AND attribute_code = '$attribute_name'")
                           ->fetchObject()
                           ->attribute_id;
      return $result;
    }
    
    
    
}
