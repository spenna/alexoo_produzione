<?php
/**
 * AdvancedInvoiceLayout Order Invoice PDF model
 *
 * @category   Vianetz
 * @package    Vianetz_AdvancedInvoiceLayout
 * @author     Christoph Massmann <C.Massmann@vianetz.com>
 * @license    http://www.vianetz.com/license
 */
class Clickshop_Pickinglist_Model_Order_Pdf_Order extends Vianetz_AdvancedInvoiceLayout_Model_Order_Pdf_Abstract
{
    public function getPdf($orders = array())
    { 
        $this->_beforeGetPdf();
        $this->_initRenderer('order');

        $this->pdf = new Zend_Pdf();
        $style = new Zend_Pdf_Style();
        $this->_setFontBold($style, 10);

        foreach ($orders as $order) {
            if ($order->getStoreId()) {
              Mage::app()->getLocale()->emulate($order->getStoreId());
            }
            $this->page = $this->pdf->newPage(self::PDF_PAGE_FORMAT);
            $this->pdf->pages[] = $this->page;
            
            $this->loadConfig($order->getStore());

            /*if ( ! $this->enabled ) {
                $mageInvoice = new Mage_Sales_Model_Order_Pdf_Invoice();
                return $mageInvoice->getPdf($orders);
            }*/

            // Add Image
            $this->insertLogo($this->page, $order->getStore());

            // Add Company address
            $this->insertAddress($this->page, $order->getStore());
 
            if ( Mage::getStoreConfig(self::XML_PATH_SALES_PDF_INVOICE_SWITCH_ADDRESSES,
$order->getStore())) {
                $this->insertShippingAndBillingAddresses($this->page, $order, false, true);
            } else {
                $this->insertShippingAndBillingAddresses($this->page, $order);
            }

            // Display Customer Email Address
            if ( Mage::getStoreConfig(self::XML_PATH_SALES_PDF_INVOICE_SHOW_CUSTOMER_EMAIL, $order->getStore()) &&
				 $order->getCustomerEmail() != "" ) {
				$this->y -=$this->fontsize_regular*self::pad_sf;
				$this->_setFontRegular($this->page);
				$this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
				$this->page->drawText(Mage::helper('sales')->__('E-Mail') . ": " .
$order->getCustomerEmail(), $this->margin_left+6, $this->y, $this->charset);
            }

            // Display tax / VAT number
            if ( Mage::getStoreConfig(self::XML_PATH_SALES_PDF_INVOICE_SHOW_TAXVAT, $order->getStore()) &&
				 $order->getCustomerTaxvat() != "" ) {
				$this->y -=$this->fontsize_regular*self::pad_sf;
				$this->_setFontRegular($this->page);
				$this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
				$this->page->drawText(Mage::helper('sales')->__('TAX/VAT Number') . ": " .
$order->getCustomerTaxvat(), $this->margin_left+6, $this->y, $this->charset);
			}
  
			$this->y -=50;          
            $this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
            $this->_setFontRegular($this->page, $this->fontsize_regular+4);
            // Invoice number
            $this->page->drawText(Mage::helper('sales')->__('Invoice # ') .
$order->getIncrementId(), $this->margin_left+6, $this->y, $this->charset);

			$this->y -=22;
			$this->_setFontRegular($this->page);
			// Invoice date
            $this->page->drawText(Mage::helper('sales')->__('Invoice Date: ') .
Mage::helper('core')->formatDate($order->getCreatedAt(), 'medium', false),
$this->margin_left+6, $this->y, $this->charset);
			
            // Add Head
            $this->insertHeader($this->page, $order);

            // Add Order
            $this->insertOrder($this->page, $order, Mage::getStoreConfigFlag(self::XML_PATH_SALES_PDF_INVOICE_PUT_ORDER_ID, $order->getStore()));

            // Add table head
            $this->_drawHeader($this->page);
            $this->y -=30;

            $this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));

            // Add body
            foreach ($order->getAllItems() as $item){
                // if ($item->getOrderItem()->getParentItem()) {
                //     continue;
                // }
                
                $shift = array();
                if ($this->y < $this->footer_y+18) {
                    // Add new table head 
                    $this->page = $this->_newPage($this->pdf, $this->page, $order->getStore());

                    $this->_drawHeader($this->page);
                    
                    $this->page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
                    $this->y -=20;
                }

                // Draw item
                //$this->_drawItem($item, $this->page, $order);
            }
            
            $this->y -= 40;
			if ($this->y < $this->footer_y+60) {
				$this->page = $this->_newPage($this->pdf, $this->page, $order->getStore());
				$font = $this->_setFontRegular($this->page, 9);
				$this->y -=40;
            }

            // Add totals
            $this->insertTotals($this->page, $order, $order->getStore());

		    if ( Mage::getStoreConfig(self::XML_PATH_SALES_PDF_INVOICE_SHOW_GIFTMSG,
$order->getStore()) ) {
                $this->y -= 40;
                // Insert Gift Message Text
                $this->insertGiftMessage($this->page, $this->pdf, $order);
            }

            // Add Customer Comments
            if (
Mage::getStoreConfig(self::XML_PATH_SALES_PDF_INVOICE_SHOW_CUSTOMER_COMMENTS,
$order->getStore())) {
                foreach ( $order->getCommentsCollection() as $comment ) {
                    if ( $comment->getIsActive() ) { 
                        $this->insertFreeText($this->page, $this->pdf, $order->getStore(),
$comment->getComment()); 
                    }
                }
            }
            
            $this->y -= 40;

            // Insert free text
            $text = Mage::getStoreConfig(self::XML_PATH_SALES_PDF_INVOICE_FREETEXT,
$order->getStore());
            $text = preg_replace('/{{invoice_date(\+\d)?}}/e',
"Mage::helper('core')->formatDate(\$order->getCreatedAt().('\\1'!=''?'\\1'.' days':''))"
    , $text);
            $this->insertFreeText($this->page, $this->pdf, $order->getStore(), $text);

			// Insert Footer
			$this->insertFooter($this->page,$order->getStore());

            if ($order->getStoreId()) {
                Mage::app()->getLocale()->revert();
            }
        }

        $this->_afterGetPdf();

        return $this->pdf;
    }

    protected function _replace_callback_invoice_date($val) {
        return Mage::helper('core')->formatDate($order->getCreatedAt());
    }
    
    /**
     * Draw Headline Of Table
     * @param Zend_Pdf_Page $page
     */
    protected function _drawHeader(Zend_Pdf_Page $page)
    {
        $font = $this->_setFontRegular($page, $this->fontsize_regular-1);
        $font = $page->getFont();
        $size = $page->getFontSize();

        #$page->setFillColor(new Zend_Pdf_Color_RGB(0.93, 0.92, 0.92));
        try {
            $page->setFillColor(new Zend_Pdf_Color_Html($this->header_color));
        }
        catch (Exception $ex) {
            $page->setFillColor(new Zend_Pdf_Color_Html($this->header_color_default));
        }    
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        #$page->drawRectangle(70, $this->y, 550, $this->y-15);
        $page->drawRectangle($this->margin_left, $this->y,
$page->getWidth()-$this->margin_right, $this->y-15);
        $this->y -=10;
        
        $page->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));
        $page->drawText(Mage::helper('sales')->__('SKU'), $this->margin_left+8, $this->y,
$this->charset);
        $page->drawText(Mage::helper('sales')->__('Product'), $this->margin_left+108,
$this->y, $this->charset);
        $page->drawText(Mage::helper('sales')->__('QTY'),
$page->getWidth()-$this->margin_right-170-$this->widthForStringUsingFontSize(Mage::helper('sales')->__('QTY'),
$font, $this->fontsize_regular), $this->y, $this->charset);
        $page->drawText(Mage::helper('sales')->__('Price'),
$page->getWidth()-$this->margin_right-100-$this->widthForStringUsingFontSize(Mage::helper('sales')->__('Price'),
$font, $this->fontsize_regular), $this->y, $this->charset);
        //$page->drawText(Mage::helper('sales')->__('Tax'), 480, $this->y, $this->charset);
        $page->drawText(Mage::helper('sales')->__('Subtotal'),
$page->getWidth()-$this->margin_right-5-$this->widthForStringUsingFontSize(Mage::helper('sales')->__('Subtotal'),
$font, $this->fontsize_regular), $this->y, $this->charset);
    }
    
}

/* vim: set ts=4 sw=4 expandtab nu tw=90: */
