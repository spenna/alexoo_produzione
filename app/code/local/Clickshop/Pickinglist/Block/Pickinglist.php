<?php
class Clickshop_Pickinglist_Block_Pickinglist extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getPickinglist()     
     { 
        if (!$this->hasData('pickinglist')) {
            $this->setData('pickinglist', Mage::registry('pickinglist'));
        }
        return $this->getData('pickinglist');
        
    }
}
