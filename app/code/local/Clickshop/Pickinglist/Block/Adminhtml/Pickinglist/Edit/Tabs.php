<?php

class Clickshop_Pickinglist_Block_Adminhtml_Pickinglist_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('pickinglist_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('pickinglist')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('pickinglist')->__('Item Information'),
          'title'     => Mage::helper('pickinglist')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('pickinglist/adminhtml_pickinglist_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}
