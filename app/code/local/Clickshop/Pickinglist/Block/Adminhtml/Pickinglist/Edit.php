<?php

class Clickshop_Pickinglist_Block_Adminhtml_Pickinglist_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'pickinglist';
        $this->_controller = 'adminhtml_pickinglist';
        
        $this->_updateButton('save', 'label', Mage::helper('pickinglist')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('pickinglist')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('pickinglist_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'pickinglist_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'pickinglist_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('pickinglist_data') && Mage::registry('pickinglist_data')->getId() ) {
            return Mage::helper('pickinglist')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('pickinglist_data')->getTitle()));
        } else {
            return Mage::helper('pickinglist')->__('Add Item');
        }
    }
}
