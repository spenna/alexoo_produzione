<?php

/**
 * Data helper
 *
 * @category    Aoe
 * @package     Aoe_Static
 * @author      Toni Grigoriu <toni@tonigrigoriu.com>
 */
class Aoe_Static_Helper_Data extends Mage_Core_Helper_Abstract
{
  
  
    /**
     * Check if varnish is enabled in Cache management.
     * 
     * @return boolean  True if varnish is enable din Cache management. 
     */
    public function useVarnishCache(){
        return Mage::app()->useCache('aoestatic');
    }
    
    /**
     * Purges all cache on all Varnish servers.
     * 
     * @return array errors if any
     */
    public function purgeAll()
    {
        return $this->purge(array('/.*'));
    }
    
    public function purge( $urls ) {
      
      if (!empty($urls)) {
        $path = Mage::getBaseDir('var') . DS . 'varnish' . DS;
        if (!is_dir($path)) mkdir($path);
        $destination =  $path.'urls.txt';
        file_put_contents( $destination, implode("\n", $urls )."\n", FILE_APPEND );
      }
      
    }
  
  
    /**
     * Check if a fullActionName is configured as cacheable
     *
     * @param string $fullActionName
     * @return false|int false if not cacheable, otherwise lifetime in seconds
     */
    public function isCacheableAction($fullActionName)
    {
        $cacheActionsString = Mage::getStoreConfig('system/aoestatic/cache_actions');
        foreach (explode(',', $cacheActionsString) as $singleActionConfiguration) {
            list($actionName, $lifeTime) = explode(';', $singleActionConfiguration);
            if (trim($actionName) == $fullActionName) {
                return intval(trim($lifeTime));
            }
        }
        return false;
	}
	
	public function _getUrlsForProduct($product) {
	  return array( $product->getUrlKey() );
	}
	
	public function _getUrlsForCategory($category) {
	  return array( $category->getUrlKey() );
	}
	
	/**
   * Returns all urls related to this cms page
   */
  public function _getUrlsForCmsPage($cmsPageId)
  {
      $urls = array();
      $page = Mage::getModel('cms/page')->load($cmsPageId);
      if ($page->getId()) {
          $urls[] = $page->getIdentifier();
          if ($page->getIdentifier() == 'home') {
            $urls[] = '/';
          }
      }

      return $urls;
  }
	
}