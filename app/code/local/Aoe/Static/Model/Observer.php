<?php
/**
 * Observer model
 *
 * @category    Aoe
 * @package     Aoe_Static
 * @author		Fabrizio Branca <mail@fabrizio-branca.de>
 * @author      Toni Grigoriu <toni@tonigrigoriu.com>
 */
class Aoe_Static_Model_Observer
{
	/**
	 * Check when varnish caching should be enabled.
	 *
	 * @param Varien_Event_Observer $observer
	 * @return Aoe_Static_Model_Observer
	 */
	public function processPreDispatch(Varien_Event_Observer $observer)
	{
	  $helper = Mage::helper('aoestatic'); /* @var $helper Aoe_Static_Helper_Data */
	  if ( $helper->useVarnishCache() ) {
    
    		$event = $observer->getEvent(); /* @var $event Varien_Event */
    		$controllerAction = $event->getControllerAction(); /* @var $controllerAction Mage_Core_Controller_Varien_Action */
    		$fullActionName = $controllerAction->getFullActionName();

    		$lifetime = $helper->isCacheableAction($fullActionName);

    		$response = $controllerAction->getResponse(); /* @var $response Mage_Core_Controller_Response_Http */
    		$response->setHeader('Access-Control-Allow-Origin', '*'); // Only for debugging and information			
		
    		if ($lifetime) {
    			// allow caching
    			$response->setHeader('X-Magento-Lifetime', $lifetime, true); // Only for debugging and information
    			$response->setHeader('Cache-Control', 'max-age='. $lifetime, true);
    			$response->setHeader('aoestatic', 'cache', true);
    		} else {
    			// do not allow caching
    			$cookie = Mage::getModel('core/cookie'); /* @var $cookie Mage_Core_Model_Cookie */

    			$name = '';
    			$loggedIn = false;
    			$session = Mage::getSingleton('customer/session'); /* @var $session Mage_Customer_Model_Session  */
    			if ($session->isLoggedIn()) {
    				$loggedIn = true;
    				$name = $session->getCustomer()->getName();
                }
    			$response->setHeader('X-Magento-LoggedIn', $loggedIn ? '1' : '0', true); // Only for debugging and information
                $cookie->set('aoestatic_customername', $name, '3600', '/');
    		}
    		$response->setHeader('X-Magento-Action', $fullActionName, true); // Only for debugging and information
    }
    
		return $this;
	}

	/**
	 * Add layout handle 'aoestatic_cacheable' or 'aoestatic_notcacheable'
	 *
	 * @param Varien_Event_Observer $observer
	 */
	public function beforeLoadLayout(Varien_Event_Observer $observer)
	{
		$helper = Mage::helper('aoestatic'); /* @var $helper Aoe_Static_Helper_Data */
		$event = $observer->getEvent(); /* @var $event Varien_Event */
		$controllerAction = $event->getAction(); /* @var $controllerAction Mage_Core_Controller_Varien_Action */
		$fullActionName = $controllerAction->getFullActionName();

		$lifetime = $helper->isCacheableAction($fullActionName);

		$handle = $lifetime ? 'aoestatic_cacheable' : 'aoestatic_notcacheable';

		$observer->getEvent()->getLayout()->getUpdate()->addHandle($handle);
	}
	
	public function onProductSave( $observer ) {
    // $product = $observer->getProduct();
    //     $this->cleanUrls( $product->getUrlKey() );
    return $this;
	}
	
	public function onStockSave( $observer ) {
	  $product = $observer->getEvent()->getItem()->getProduct();
    $this->cleanUrls( $product->getUrlKey() );
    return $this;
	}
	
	public function salesOrderItemSaveAfterOrderNew( Varien_Event_Observer $observer ) {
    $orderItem = $observer->getEvent()->getItem();
    $_product = Mage::getModel('catalog/product')->load( $orderItem->getProductId() );
    $urls = Mage::helper('aoestatic')->_getUrlsForProduct($_product);
    $this->cleanUrls($urls);
	  return $this;
	}
	
	public function purgeCache($observer)
  {   
      $tags = $observer->getTags();
      $urls = array();
      
      if ($tags == array()) {
          $errors = Mage::helper('aoestatic')->purgeAll();
          if (!empty($errors)) {
              Mage::getSingleton('adminhtml/session')->addError("Varnish Purge failed");
          } else {
              Mage::getSingleton('adminhtml/session')->addSuccess("The Varnish cache storage has been flushed.");
          }
          return;
      }
      
      // compute the urls for affected entities 
      foreach ((array)$tags as $tag) {
          //catalog_product_100 or catalog_category_186
          $tag_fields = explode('_', $tag);
          if (count($tag_fields)==3) {
              if ($tag_fields[1]=='product') {
                  // Mage::log("Purge urls for product " . $tag_fields[2]);

                  // get urls for product
                  $product = Mage::getModel('catalog/product')->load($tag_fields[2]);
                  $urls = array_merge($urls, Mage::helper('aoestatic')->_getUrlsForProduct($product));
              } elseif ($tag_fields[1]=='category') {
                  // Mage::log('Purge urls for category ' . $tag_fields[2]);

                  $category = Mage::getModel('catalog/category')->load($tag_fields[2]);
                  $category_urls = Mage::helper('aoestatic')->_getUrlsForCategory($category);
                  $urls = array_merge($urls, $category_urls);
              } elseif ($tag_fields[1]=='page') {
                  $urls = Mage::helper('aoestatic')->_getUrlsForCmsPage($tag_fields[2]);
              }
          }
      }
      

      $this->cleanUrls($urls);
      return $this;
  }
  
  public function cleanUrls( $urls ) {
    Mage::helper('aoestatic')->purge( $urls );
  }
}
