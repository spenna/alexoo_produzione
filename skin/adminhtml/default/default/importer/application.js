/*
 * jQuery File Upload Plugin JS Example 5.0.2
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://creativecommons.org/licenses/MIT/
 */

/*jslint nomen: true */
/*global $ */

jQuery(function () {		
    // Initialize the jQuery File Upload widget:
    jQuery('.fileupload').fileupload({
			dataType: 'json',
      url: IMPORTER_BASE_URL+'upload.php',
      start: function(data) {
        jQuery('.save').attr('disabled', 'disabled');
        jQuery(this).find('.importer_loader').show();
      },
      // add: function(e, data) {
      //   data.submit();
      //   return true;
      // },
      done: function (e, data) {
          var images_string = jQuery('#images_extra').val();
          var images = images_string.split(';');
          jQuery.each(data.result, function (index, file) {
            images.push( file.name );
							//jQuery('<p/>').text( file.name + ' importato.').appendTo('.importer-messages');
          });
          
          images_string = images.join(';');
          jQuery('#images_extra').val( images_string );
          jQuery('.save').attr('disabled', false );
          jQuery(this).find('.importer_loader').hide();
          
      }
		});
});