function update_blocks() {
	
	var data = { getBlocks: {} };

	// add placeholders
	jQuery('.placeholder').each(function() {
		data.getBlocks[ jQuery(this).attr('id') ] = jQuery(this).attr('rel');
	});

	// add current product
	if (typeof CURRENTPRODUCTID !== 'undefined' && CURRENTPRODUCTID) {
		data.currentProductId = CURRENTPRODUCTID;
	}
	
	jQuery.get(
		AJAXHOME_URL,
		data,
		function (data) {
			for(var id in data.blocks) {
				jQuery('#' + id).html(data.blocks[id]);
			}
			if ( data.is_in_stock != undefined ) {
				if (data.is_in_stock == 1) {
					jQuery('#product_type_data').html('<p class="availability in-stock">Disponibilità: <span>Disponibile</span></p>');
				} else {
					jQuery('#product_type_data').html('<p class="availability out-of-stock">Disponibilità: <span>Esaurito</span></p>');
					jQuery('.btn-cart').hide();
				}
			};
			jQuery.cookie('frontend', data.sid, { path: '/' });
		},
		'json'
	);
}

jQuery(document).ready(function() {
	jQuery('.block-layered-nav dt').hover(
		function() {
			index = jQuery('dt').index( jQuery(this) );
			block = jQuery(this).parent().find('dd');
			block.css({ left: index * 210 });
			block.removeClass('active');
			jQuery(this).next().addClass('active');
		},
		function() {
			
		}
	);
	
	jQuery('.block-layered-nav dd').hover(
		function() {},
		function() { 
			jQuery(this).removeClass('active');
		}
	);
	
	jQuery('.nav-container').hover(
		function() { jQuery('.block-layered-nav dd').removeClass('active') },
		function() { }
	);
		
	
		
	jQuery('.css-tabs a').click( function(event) {
		event.preventDefault();
		var index = jQuery('.css-tabs a').index(jQuery(this));
		jQuery('.css-tabs a').removeClass('current');
		jQuery('.css-panes div.pane').removeClass('active');
		jQuery('.css-tabs a').eq(index).addClass('current');
		jQuery('.css-panes div.pane').eq(index).addClass('active');
	});
	
	
	jQuery('#nav a').each(function( i, item ) {
		href = jQuery(this).attr('href');
        if ( href.indexOf('?') == -1 ) {
            href = href + '?dir=desc&order=position'; //ordinamento decrescente
			//href = href + '?dir=asc&order=position'; //ordinamento crescente
	        jQuery(this).attr('href',href);
        } 
	});
	

	// E.T. phone home
	update_blocks();

});