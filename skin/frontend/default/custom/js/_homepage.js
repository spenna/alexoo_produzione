var fade_interval = 4000;
var fade_velocity = 1000;
var scroll_velocity = 1000;
var t;


check_arrows = function( container ) {
  scrolling_index = parseInt(container.attr('index'));
  scrolling_banner_total = parseInt(container.attr('total') );
  
	if ( scrolling_index > 0 ) {
		container.find('.arrow_left').show();
	} else {
		container.find('.arrow_left').hide();
	}
	
	if ( scrolling_index < (scrolling_banner_total-1) && scrolling_banner_total > 1 ) {
		container.find('.arrow_right').show();
	} else {
		container.find('.arrow_right').hide();
	}
}


jQuery(document).ready(function() {

	
	jQuery('.home_banners .arrow_left, .home_banners .arrow_right').click( function(event) {
		event.preventDefault();
		container = jQuery(this).parent();
		if (jQuery(this).hasClass('arrow_left')) {
			container.find('ul').eq(0).stop(false,true).animate({
				left: '+=960'
			}, scroll_velocity, function() {
				container.attr('index', parseInt(container.attr('index')) - 1 );  
				check_arrows( container );
			});
		} else {
			container.find('ul').eq(0).stop(false,true).animate({
				left: '-=960'
			}, scroll_velocity, function() {
				container.attr('index', parseInt(container.attr('index')) + 1 );  
				check_arrows( container );
			});
		}
	});
	
});