<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);


$mageFilename = dirname(__FILE__).'/../app/Mage.php';
require_once $mageFilename;
require_once dirname(__FILE__)."/BaseComparator.class.php";


umask(0);
Mage::app('default');
Mage::app()->loadAreaPart(Mage_Core_Model_App_Area::AREA_FRONTEND,Mage_Core_Model_App_Area::PART_EVENTS);

class Shopalike extends BaseComparator
{
  static $attribute_color_id;
  static $attribute_brand_id;
  static $attribute_material_id;
  static $attribute_size_id;
  protected $destination;
  
  public function __construct( $destination = null )
  {
    $this->destination = $destination;
    
    self::$attribute_color_id = self::getAttributeEAV_id('multicolor');
    self::$attribute_brand_id = self::getAttributeEAV_id('manufacturer');
    self::$attribute_material_id = self::getAttributeEAV_id('material');
    self::$attribute_size_id = self::getAttributeEAV_id('size');
    
    $this->loadCategories();
    $this->loadAttributes( array('multicolor','manufacturer', 'material', 'size') );
    
    $this->startExport();
  }
  
  
  public function startExport() {
    $stockCollection = Mage::getModel('cataloginventory/stock_item')->getCollection()->addFieldToFilter('is_in_stock', 1);

    $productIds = array();

    foreach ($stockCollection as $item) {
        $productIds[] = $item->getOrigData('product_id');
    }
    

    $products = Mage::getModel('catalog/product')->getCollection();
    $products->addAttributeToSelect('id');
    $products->addAttributeToFilter('type_id', array('eq' => 'configurable'));
    $products->addIdFilter($productIds);
    $products->addAttributeToFilter( 'status', 1 );//enabled

    $output = '<?xml version="1.0" encoding="utf-8"?>';
    $output .= "\n";
    $output .= "<items>";

    foreach ( $products as $_product ) {
      $product = Mage::getModel('catalog/product')->load( $_product->getId() );
      $output .= $this->getXmlItem( $product );
    }

    $output .= "</items>";
    
    $base_dir = Mage::getBaseDir('var') . DS . 'export'. DS;
    file_put_contents( $base_dir.$this->destination, $output );
  }
  
  
  function getXmlItem( $product ) {
    $categoryIds = $product->getCategoryIds();
    $full_category = array();
    foreach ($categoryIds as $category_id ) {
      if (self::$categories[ $category_id ]['level'] > 2 ) {
        $full_category[ self::$categories[ $category_id ]['level'] ] = self::$categories[ $category_id ]['name'];
      }
    }
    ksort( $full_category );
    
    $sizes = array();
    $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$product);
    foreach ($childProducts as $childProduct) {
      $child = Mage::getModel('catalog/product')->load( $childProduct->getId() );
      $sizes[ ] = self::$attributes[ 'size' ][ $child->getSize() ];
    }
    sort($sizes);
    
    $colors = array();
    foreach (explode(',',$product->getMulticolor()) as $color_id) {
      $colors[] = self::$attributes[ 'multicolor' ][ $color_id ];
    }
    sort($colors);

    $xml = sprintf(<<<EOF
      <item>
    		<itemId>%s</itemId>
    		<name>%s</name>
    		<fullCategory>%s</fullCategory>
    		<category>%s</category>
    		<gender>Female</gender>
    		<color>%s</color>
    		<brand>%s</brand>
    		<material>%s</material>
    		<description><![CDATA[%s]]></description>
    		<price>%s</price>%s
    		<currency>EUR</currency>
    		<availability>in stock</availability>
    		<shippingCosts>0</shippingCosts>
    		<sizes>%s</sizes>
    		<image>
    			%s
    		</image>
    		<deepLink>
    			%s
    		</deepLink>
    		<lastModified>%s</lastModified>
    	</item>
EOF
      ,
      $product->getSku(),
      $product->getName(),
      implode('|', $full_category ),
      array_pop($full_category), //count($full_category) > 0 ? $full_category[ count($full_category) - 1 ] : '',
      implode(',',$colors),
      self::$attributes[ 'manufacturer' ][ $product->getManufacturer() ],
      self::$attributes[ 'material' ][ $product->getMaterial() ],
      $product->getDescription(),
      number_format( $product->getFinalPrice(), 2 ),
      $product->getPrice() != $product->getFinalPrice() ? "\n\t\t\t".'<oldPrice>'.number_format($product->getPrice(),2).'</oldPrice>' : '',
      //$product->getStockItem()->getQty(),
      count($sizes) > 1 ? $sizes[0].'-'.$sizes[ count($sizes)-1 ] : '',
      (string)Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'catalog/product'.$product->getImage(),
      $product->getProductUrl(),
      $product->getUpdatedAt()
    );
    $xml .= "\n";
    return $xml;
  }
  
}


$service = new Shopalike( 'shopalike.xml' );