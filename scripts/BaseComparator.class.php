<?php

class BaseComparator
{
  
  static $categories;
  static $attributes;
  
  public static function loadCategories() {
		$category = Mage::getModel ( 'catalog/category' );
		$tree = $category->getTreeModel();
		$tree->load();
		$ids = $tree->getCollection()->getAllIds();

		
		$categories = array();
		
		foreach ($ids as $id) {
			$cat = Mage::getModel('catalog/category')->load($id);
			$categories[$id] = array('name' => $cat->getName(), 'parent' => $cat->getParentIds(), 'level' => $cat->getLevel() );
		}
		
		self::$categories = $categories;
	}
	
	
	public function loadAttributes( $list ) {
	  foreach ($list as $item) {
	    $attribute_id = self::getAttributeEAV_id( $item );
      self::$attributes[ $item ] = self::getAttributeValues( $attribute_id );
	  }
	}
  
  
  /**
   * Retrieve all values of a specific dropdown attribute 
   *
   * @param string $attribute_id 
   * @return array
   * @author Andrea Restello
   */
  public static function getAttributeValues( $attribute_id ) {
    $attribute_collection = Mage::getModel('eav/config')->getAttribute('catalog_product', $attribute_id );
		$attrubuteArray = array();
		foreach ( $attribute_collection->getSource()->getAllOptions(true, true) as $option) {
			$attrubuteArray[$option['value']] = $option['label'];
		}
		return $attrubuteArray;
  }
  
  
  /**
   * Retrieve entity_id given entity name of an attribute
   *
   * @author Andrea Restello
   * @return int
   */
  public static function getAttributeEAV_id ( $attribute_name )
  {
  	$connection = Mage::getSingleton('core/resource')->getConnection('core_read');
    $result = $connection->query("SELECT attribute_id FROM eav_attribute WHERE entity_type_id = 4 AND attribute_code = '$attribute_name'")
                         ->fetchObject()
                         ->attribute_id;
    return $result;
  }
  
  
}
